const $ = require('jquery');

export function main() {
    popover();
    messageBox();
};

function popover() {
    $(document).ready(function() {
        $('[data-toggle="popover"]').popover();
    });
}

function messageBox() {
    $(document).ready(function() {
        $('[data-toggle="confirmation"][data-message]').on('click', function (event) {
            var message = $(this).data('message');

            return confirm(message) || event.preventDefault();
        });
    });
}
