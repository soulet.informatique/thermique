<?php

namespace App\Repository;

use App\Entity\coOwnerShip\Building as Building;
use App\Entity\coOwnerShip\CoOwnership;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query;

/**
 * @method Building|null find($id, $lockMode = null, $lockVersion = null)
 * @method Building|null findOneBy(array $criteria, array $orderBy = null)
 * @method Building[]    findAll()
 * @method Building[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BuildingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Building::class);
    }

    protected function getQbBuilding(CoOwnership $coOwnership, ?string $name)
    {
        return $this->createQueryBuilder('b')
            ->where('b.name = :name')
            ->setParameter('name', $name)
            ->andWhere('b.coOwnership = :coOwnership')
            ->setParameter('coOwnership', $coOwnership);
    }

    public function isBuildingExist(CoOwnership $coOwnership, ?string $name) {
      $qb = $this->getQbBuilding($coOwnership, $name);
      $qb->select('count(b.name)');

      $result = $qb->getQuery()->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);

      return $result > 0;
    }

    public function getBuildingByName(CoOwnership $coOwnership, ?string $name) {
        $qb = $this->getQbBuilding($coOwnership, $name);
        $res = $qb->getQuery()
                 ->getResult();

        if(count($res) > 0) {
            return $res['0'];
        }
      return null;
    }

    public function findNameByCoOwnershipId(CoOwnership $coOwnership) {
      $qb = $this->createQueryBuilder('b')
                 ->orderBy('b.name', 'ASC')
                 ->andWhere('b.coOwnership = :coOwnership')
                 ->setParameter('coOwnership', $coOwnership);

      return $qb;
    }

    public function findBuildingByCoOwnership(CoOwnership $coOwnership) {
      $qb = $this->createQueryBuilder('b')
            ->andWhere('b.coOwnership = :coOwnership')
            ->setParameter('coOwnership', $coOwnership)
            ->orderBy('b.buildingNumber', 'ASC')
            ->addOrderBy('b.name', 'ASC')
            ->getQuery();

      return $qb;
    }


    // /**
    //  * @return Building[] Returns an array of Building objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Building
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
