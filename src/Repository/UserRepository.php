<?php

namespace App\Repository;

use App\Entity\User\User;
use App\Entity\CoOwnerShip\Owner;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query;

/**
 * @method Export|null find($id, $lockMode = null, $lockVersion = null)
 * @method Export|null findOneBy(array $criteria, array $orderBy = null)
 * @method Export[]    findAll()
 * @method Export[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findUsers() {
      $qb = $this->createQueryBuilder('u')
            ->andWhere("u.roles LIKE '%ROLE_MANAGER%' OR u.roles LIKE '%ROLE_ADMIN%'")
            ->getQuery();

      return $qb;
    }

    public function findUserById(int $id) {
      $qb = $this->createQueryBuilder('u')
            ->andWhere('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

      return $qb;
    }

    public function contains(Owner $owner) {
      $qb = $this->createQueryBuilder('u')
      ->select('count(u.owner)')
      ->where('u.owner = :owner')
      ->setParameter('owner', $owner);

      $result = $qb->getQuery()->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);

      return $result > 0;
    }

}
