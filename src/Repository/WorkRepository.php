<?php

namespace App\Repository;

use App\Entity\Scenario\Work;
use App\Entity\CoOwnerShip\CoOwnership;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query;

/**
 * @method Work|null find($id, $lockMode = null, $lockVersion = null)
 * @method Work|null findOneBy(array $criteria, array $orderBy = null)
 * @method Work[]    findAll()
 * @method Work[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Work::class);
    }

    protected function getQbWork(CoOwnership $coOwnership, ?string $name)
    {
        return $this->createQueryBuilder('w')
            ->leftJoin('w.workHasBuildings', 'wb')
            ->leftJoin('wb.building', 'b')
            ->where('w.name = :name')
            ->setParameter('name', $name)
            ->andWhere('b.coOwnership = :coOwnership')
            ->setParameter('coOwnership', $coOwnership);
    }

    public function findWorkNameByCoOwnership(CoOwnership $coOwnership) {
      $qb = $this->createQueryBuilder('w')
            ->addSelect('wb, b')
            ->leftJoin('w.workHasBuildings', 'wb')
            ->leftJoin('wb.building', 'b')
            ->orderBy('w.name', 'ASC')
            ->andWhere('b.coOwnership = :coOwnership')
            ->setParameter('coOwnership', $coOwnership);

      return $qb;
    }

    public function findWorksByCoOwnership(CoOwnership $coOwnership) {
      $qb = $this->createQueryBuilder('w')
            ->addSelect('wb, b')
            ->leftJoin('w.workHasBuildings', 'wb')
            ->leftJoin('wb.building', 'b')
            ->andWhere('b.coOwnership = :coOwnership')
            ->setParameter('coOwnership', $coOwnership)
            ->getQuery();

      return $qb;
    }


    // /**
    //  * @return Work[] Returns an array of Work objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Work
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    
    public function isWorkExist(CoOwnership $coOwnership, ?string $name) {
        $qb = $this->getQbWork($coOwnership, $name);
        $qb->select('count(b.name)');

        $result = $qb->getQuery()->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);

        return $result > 0;
    }

    public function getWorkByName(CoOwnership $coOwnership, ?string $name) {
        $qb = $this->getQbWork($coOwnership, $name);
        $res = $qb->getQuery()
            ->getResult();

        if(count($res) > 0) {
            return $res['0'];
        }
        return null;
    }
}
