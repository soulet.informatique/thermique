<?php

namespace App\Repository;

use App\Entity\CoOwnerShip\CoOwnership;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CoOwnership|null find($id, $lockMode = null, $lockVersion = null)
 * @method CoOwnership|null findOneBy(array $criteria, array $orderBy = null)
 * @method CoOwnership[]    findAll()
 * @method CoOwnership[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoOwnershipRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CoOwnership::class);
    }

    public function findNotArchivedValues() {
      $qb = $this->createQueryBuilder('c')
                 ->orderBy('c.id', 'DESC')
                 ->where('c.archived = false');
      return $qb;
    }

    public function findNotArchivedValueById(int $id) {
      $qb = $this->createQueryBuilder('c')
            ->where('c.archived = false')
            ->andWhere('c.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

      return $qb;
    }

    public function findArchivedValues() {
      $qb = $this->createQueryBuilder('c')
                 ->orderBy('c.id', 'DESC')
                 ->where('c.archived = true');
      return $qb;
    }

    public function findArchivedValueById(int $id) {
      $qb = $this->createQueryBuilder('c')
            ->where('c.archived = true')
            ->andWhere('c.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

      return $qb;
    }

    public function findCoOwnershipById(int $id) {
      $qb = $this->createQueryBuilder('c')
                 ->andWhere('c.id = :id')
                 ->setParameter('id', $id)
                 ->getQuery()
                 ->getOneOrNullResult();

      return $qb;
    }

    // /**
    //  * @return CoOwnership[] Returns an array of CoOwnership objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CoOwnership
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
