<?php

namespace App\Repository;

use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\CoOwnerShip\Owner;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Owner|null find($id, $lockMode = null, $lockVersion = null)
 * @method Owner|null findOneBy(array $criteria, array $orderBy = null)
 * @method Owner[]    findAll()
 * @method Owner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OwnerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Owner::class);
    }

    public function findOwnerByCoOwnership(CoOwnership $coOwnership, $canGrant = false) {
      $qb = $this->createQueryBuilder('o')
            ->addSelect('l, b')
            ->leftJoin('o.user', 'u')
            ->leftJoin('o.lots', 'l')
            ->leftJoin('l.building', 'b')
            ->andWhere('b.coOwnership = :coOwnership')
            ->setParameter('coOwnership', $coOwnership);
      if($canGrant) {
          $qb->andWhere('o.maritalStatus IS NOT NULL');
      }

      return $qb;
    }


    // /**
    //  * @return Owner[] Returns an array of Owner objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Owner
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
