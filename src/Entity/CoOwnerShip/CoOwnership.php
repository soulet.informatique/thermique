<?php

namespace App\Entity\CoOwnerShip;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\CoOwnerShip\Building;
use App\Entity\Grant\Grant;
use App\Entity\Document\DocumentCategory;
use Doctrine\Common\Collections\Criteria;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CoOwnershipRepository")
 */
class CoOwnership
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $businessNumber;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $residenceName;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $residenceType;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $postalCode;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $department;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $climaticArea;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $productionType;

    /**
     * @Assert\Date
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"grant"})
     * @Type("DateTime<'Y-m-d'>")
     */
    private $constructionDate;

    /**
     * @Assert\Type("boolean")
     *
     * @ORM\Column(type="boolean")
     */
    private $coOwnershipFragile;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"grant"})
     */
    private $worksFund;

    /**
     * @Assert\Date
     * @ORM\Column(type="date", nullable=true)
     */
    private $surveyOpeningDate;

    /**
     * @Assert\Date
     * @ORM\Column(type="date", nullable=true)
     */
    private $surveyClosingDate;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre.")
     * @Assert\GreaterThan(
     *     0
     * )
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"grant"})
     */
    private $numberOfHousings;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $livingSpace;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"grant"})
     */
    private $numberOfShops;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"grant"})
     */
    private $numberOfOffices;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $livingSpaceAverage;

    /**
     * @Assert\Range(min=0, max=100)
     *
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $projectFees;

    /**
     * @Assert\Date
     * @ORM\Column(type="date", nullable=true)
     */
    private $auditInitialDate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $auditDoneByOffice;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $energyInitialTag;

    /**
     * @Assert\Type("boolean")
     *
     * @ORM\Column(type="boolean")
     */
    private $exonerationPropertyTax;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tantiemesTotal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @Assert\Type("boolean")
     *
     * @ORM\Column(type="boolean")
     */
    private $archived = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $blockAccounts;

    /**
     * @ORM\OneToMany(targetEntity="Building", mappedBy="coOwnership")
     * @ORM\OrderBy({"buildingNumber" = "ASC", "name" = "ASC"})
     */
    private $buildings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Grant\Grant", mappedBy="coOwnership")
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $grants;

    /**
     * Used to store attachedFiles
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Document\UploadedFile", mappedBy="coOwnership", cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $attachedFiles;

    public function __construct()
    {
        $this->buildings = new ArrayCollection();
        $this->grants = new ArrayCollection();
        $this->attachedFiles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setNumberOfShops(0);
        $this->setNumberOfOffices(0);
        $this->blockAccounts = false;
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBusinessNumber(): ?string
    {
        return $this->businessNumber;
    }

    public function setBusinessNumber(string $businessNumber): self
    {
        $this->businessNumber = $businessNumber;

        return $this;
    }

    public function getResidenceName(): ?string
    {
        return $this->residenceName;
    }

    public function setResidenceName(string $residenceName): self
    {
        $this->residenceName = $residenceName;

        return $this;
    }

    public function getResidenceType(): ?string
    {
        return $this->residenceType;
    }

    public function setResidenceType(string $residenceType): self
    {
        $this->residenceType = $residenceType;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getDepartment(): ?int
    {
        return $this->department;
    }

    public function setDepartment(int $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getClimaticArea(): ?string
    {
        return $this->climaticArea;
    }

    public function setClimaticArea(string $climaticArea): self
    {
        $this->climaticArea = $climaticArea;

        return $this;
    }

    public function getProductionType(): ?string
    {
        return $this->productionType;
    }

    public function setProductionType(string $productionType): self
    {
        $this->productionType = $productionType;

        return $this;
    }

    public function getConstructionDate(): ?\DateTimeInterface
    {
        return $this->constructionDate;
    }

    public function setConstructionDate(\DateTimeInterface $constructionDate): self
    {
        $this->constructionDate = $constructionDate;

        return $this;
    }

    public function getCoOwnershipFragile(): ?bool
    {
        return $this->coOwnershipFragile;
    }

    public function setCoOwnershipFragile(bool $coOwnershipFragile): self
    {
        $this->coOwnershipFragile = $coOwnershipFragile;

        return $this;
    }

    public function getWorksFund(): ?int
    {
        return $this->worksFund;
    }

    public function setWorksFund(int $worksFund): self
    {
        $this->worksFund = $worksFund;

        return $this;
    }

    public function getSurveyOpeningDate(): ?\DateTimeInterface
    {
        return $this->surveyOpeningDate;
    }

    public function setSurveyOpeningDate(\DateTimeInterface $surveyOpeningDate): self
    {
        $this->surveyOpeningDate = $surveyOpeningDate;

        return $this;
    }

    public function getSurveyClosingDate(): ?\DateTimeInterface
    {
        return $this->surveyClosingDate;
    }

    public function setSurveyClosingDate(\DateTimeInterface $surveyClosingDate): self
    {
        $this->surveyClosingDate = $surveyClosingDate;

        return $this;
    }

    public function getNumberOfHousings(): ?int
    {
        return $this->numberOfHousings;
    }

    public function setNumberOfHousings(int $numberOfHousings): self
    {
        $this->numberOfHousings = $numberOfHousings;

        return $this;
    }

    public function getLivingSpace(): ?int
    {
        return $this->livingSpace;
    }

    public function setLivingSpace(int $livingSpace): self
    {
        $this->livingSpace = $livingSpace;

        return $this;
    }

    public function getNumberOfShops(): ?int
    {
        return $this->numberOfShops;
    }

    public function setNumberOfShops(int $numberOfShops): self
    {
        $this->numberOfShops = $numberOfShops;

        return $this;
    }

    public function getNumberOfOffices(): ?int
    {
        return $this->numberOfOffices;
    }

    public function setNumberOfOffices(int $numberOfOffices): self
    {
        $this->numberOfOffices = $numberOfOffices;

        return $this;
    }

    public function getLivingSpaceAverage(): ?int
    {
        return $this->livingSpaceAverage;
    }

    public function setLivingSpaceAverage(int $livingSpaceAverage): self
    {
        $this->livingSpaceAverage = $livingSpaceAverage;

        return $this;
    }

    public function getProjectFees(): ?string
    {
        return $this->projectFees;
    }

    public function setProjectFees(string $projectFees): self
    {
        $this->projectFees = $projectFees;

        return $this;
    }

    public function getAuditInitialDate(): ?\DateTimeInterface
    {
        return $this->auditInitialDate;
    }

    public function setAuditInitialDate(\DateTimeInterface $auditInitialDate): self
    {
        $this->auditInitialDate = $auditInitialDate;

        return $this;
    }

    public function getAuditDoneByOffice(): ?bool
    {
        return $this->auditDoneByOffice;
    }

    public function setAuditDoneByOffice(bool $auditDoneByOffice): self
    {
        $this->auditDoneByOffice = $auditDoneByOffice;

        return $this;
    }

    public function getEnergyInitialTag(): ?string
    {
        return $this->energyInitialTag;
    }

    public function setEnergyInitialTag(string $energyInitialTag): self
    {
        $this->energyInitialTag = $energyInitialTag;

        return $this;
    }

    public function getExonerationPropertyTax(): ?bool
    {
        return $this->exonerationPropertyTax;
    }

    public function setExonerationPropertyTax(bool $exonerationPropertyTax): self
    {
        $this->exonerationPropertyTax = $exonerationPropertyTax;

        return $this;
    }

    public function getTantiemesTotal(): ?int
    {
        return $this->tantiemesTotal;
    }

    public function setTantiemesTotal(int $tantiemesTotal): self
    {
        $this->tantiemesTotal = $tantiemesTotal;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getArchived(): ?bool
    {
        return $this->archived;
    }

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function getBlockAccounts(): ?bool
    {
        return $this->blockAccounts;
    }

    public function setBlockAccounts(bool $blockAccounts): self
    {
        $this->blockAccounts = $blockAccounts;

        return $this;
    }

    /**
     * @return Collection|Building[]
     */
    public function getBuildings(): Collection
    {
        return $this->buildings;
    }

    public function addBuilding(Building $building): self
    {
        if (!$this->buildings->contains($building)) {
            $this->buildings[] = $building;
            $building->setCoOwnership($this);
        }

        return $this;
    }

    public function removeBuilding(Building $building): self
    {
        if ($this->buildings->contains($building)) {
            $this->buildings->removeElement($building);
            // set the owning side to null (unless already changed)
            if ($building->getCoOwnership() === $this) {
                $building->setCoOwnership(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Grant[]
     */
    public function getGrants(): Collection
    {
        return $this->grants;
    }

    /**
     * @return Collection|Grant[]
     */
    public function getValidGrants(Owner $owner): Collection
    {
        $grants = new ArrayCollection();
        foreach($this->getGrants() as $grant) {
            if($grant->getPopulation()) {
                switch($grant->getPopulation()) {
                    case DocumentCategory::POPULATION_TYPE_FORM:
                        if($owner->isForm()) {
                            $grants[] = $grant;
                        }
                        break;
                    case DocumentCategory::POPULATION_TYPE_ALL:
                        $grants[] = $grant;
                        break;
                    case DocumentCategory::POPULATION_TYPE_NO_FORM:
                        if(!$owner->isForm()) {
                            $grants[] = $grant;
                        }
                        break;
                }
            } else {
                $grants[] = $grant;
            }
        }
        return $grants;
    }

    public function addGrant(Grant $grant): self
    {
        if (!$this->grants->contains($grant)) {
            $this->grants[] = $grant;
            $grant->setCoOwnership($this);
        }

        return $this;
    }

    public function removeGrant(Grant $grant): self
    {
        if ($this->grants->contains($grant)) {
            $this->grants->removeElement($grant);
            // set the owning side to null (unless already changed)
            if ($grant->getCoOwnership() === $this) {
                $grant->setCoOwnership(null);
            }
        }

        return $this;
    }

    public function getLots() {
      $lotsCount = 0;
      foreach ($this->getBuildings() as $building){
          $lotsCount += count($building->getLots());
      }
      return $lotsCount;
    }

    public function getOwnersCount() {
      $ownerCount = 0;
      foreach($this->getBuildings() as $building) {
        foreach($building->getLots() as $lot) {
          $ownerCount += count($lot->getOwners());
        }
      }

      return $ownerCount;
    }

    /**
     * @return Collection|Owner[]
     */
    public function getOwners(): Collection {
        $owners = new ArrayCollection();
        foreach($this->getBuildings() as $building) {
            foreach($building->getLots() as $lot) {
                foreach($lot->getOwners() as $owner) {
                    $owners[] = $owner;
                }
            }
        }
        return $owners;
    }

    /**
     * @return Collection|Owner[]
     */
    public function getCompleteOwners(DocumentCategory $category = null): Collection {
        $population = DocumentCategory::POPULATION_TYPE_FORM;
        if($category and $category->getPopulation()) {
            $population = $category->getPopulation();
        }

        $owners = new ArrayCollection();
        foreach($this->getBuildings() as $building) {
            foreach($building->getLots() as $lot) {
                foreach($lot->getOwners() as $owner) {
                    switch($population) {
                        case DocumentCategory::POPULATION_TYPE_FORM:
                            if($owner->isForm()) {
                                $owners[] = $owner;
                            }
                            break;
                        case DocumentCategory::POPULATION_TYPE_ALL:
                            $owners[] = $owner;
                            break;
                        case DocumentCategory::POPULATION_TYPE_NO_FORM:
                            if(!$owner->isForm()) {
                                $owners[] = $owner;
                            }
                            break;
                    }
                }
            }
        }
        return $owners;
    }

    public function __toString() {
      return $this->getResidenceName();
    }

    /**
     * Add attachedFile
     *
     * @param \App\Entity\Document\UploadedFile $attachedFile
     *
     * @return CoOwnership
     */
    public function addAttachedFile(\App\Entity\Document\UploadedFile $attachedFile)
    {
        $this->attachedFiles[] = $attachedFile;
        return $this;
    }

    /**
     * Remove attachedFile
     *
     * @param \App\Entity\Document\UploadedFile $attachedFile
     */
    public function removeAttachedFile(\App\Entity\Document\UploadedFile $attachedFile)
    {
        $this->attachedFiles->removeElement($attachedFile);
    }

    /**
     * Get attachedFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachedFiles()
    {
        $attachedFiles = $this->attachedFiles->toArray();;
        usort($attachedFiles, function($a, $b) {
            return ($a->getLastUpdate() > $b->getLastUpdate()) ? -1 : 1;
        });
        return $attachedFiles;
    }
}
