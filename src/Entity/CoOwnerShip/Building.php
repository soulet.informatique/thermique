<?php

namespace App\Entity\CoOwnerShip;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\CoOwnerShip\Lot as Lot;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Scenario\WorkHasBuilding as WorkHasBuilding;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass="App\Repository\BuildingRepository")
 */
class Building
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Veuillez entrer le nom du bâtiment")
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $buildingNumber;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $stair;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"grant"})
     */
    private $tantiemes;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tantiemesGarageBox;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tantiemesShop;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $others;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tantiemesHeating;

    /**
     * @ORM\OneToMany(targetEntity="Lot", mappedBy="building")
     */
    private $lots;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Scenario\WorkHasBuilding", mappedBy="building", cascade={"persist"})
     */
    private $workHasBuildings;

    /**
     * @ORM\ManyToOne(targetEntity="CoOwnership", inversedBy="buildings")
     */
    private $coOwnership;

    public function __construct()
    {
        $this->lots = new ArrayCollection();
        $this->workHasBuildings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBuildingNumber(): ?int
    {
        return $this->buildingNumber;
    }

    public function setBuildingNumber(int $buildingNumber): self
    {
        $this->buildingNumber = $buildingNumber;

        return $this;
    }

    public function getStair(): ?string
    {
        return $this->stair;
    }

    public function setStair(string $stair): self
    {
        $this->stair = $stair;

        return $this;
    }

    public function getTantiemes(): ?int
    {
        return $this->tantiemes;
    }

    public function setTantiemes(int $tantiemes): self
    {
        $this->tantiemes = $tantiemes;

        return $this;
    }

    public function getTantiemesGarageBox(): ?int
    {
        return $this->tantiemesGarageBox;
    }

    public function setTantiemesGarageBox(int $tantiemesGarageBox): self
    {
        $this->tantiemesGarageBox = $tantiemesGarageBox;

        return $this;
    }

    public function getTantiemesShop(): ?int
    {
        return $this->tantiemesShop;
    }

    public function setTantiemesShop(int $tantiemesShop): self
    {
        $this->tantiemesShop = $tantiemesShop;

        return $this;
    }

    public function getOthers(): ?int
    {
        return $this->others;
    }

    public function setOthers(int $others): self
    {
        $this->others = $others;

        return $this;
    }

    public function getTantiemesHeating(): ?int
    {
        return $this->tantiemesHeating;
    }

    public function setTantiemesHeating(int $tantiemesHeating): self
    {
        $this->tantiemesHeating = $tantiemesHeating;

        return $this;
    }

    /**
     * @return Collection|Lot[]
     */
    public function getLots(): Collection
    {
        return $this->lots;
    }

    public function addLot(Lot $lot): self
    {
        if (!$this->lots->contains($lot)) {
            $this->lots[] = $lot;
            $lot->setBuilding($this);
        }

        return $this;
    }

    public function removeLot(Lot $lot): self
    {
        if ($this->lots->contains($lot)) {
            $this->lots->removeElement($lot);
            // set the owning side to null (unless already changed)
            if ($lot->getBuilding() === $this) {
                $lot->setBuilding(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|WorkHasBuilding[]
     */
    public function getWorkHasBuildings(): Collection
    {
        return $this->workHasBuildings;
    }

    public function addWorkHasBuilding(WorkHasBuilding $workHasBuilding): self
    {
        if (!$this->workHasBuildings->contains($workHasBuilding)) {
            $this->workHasBuildings[] = $workHasBuilding;
            $workHasBuilding->setBuilding($this);
        }

        return $this;
    }

    public function removeWorkHasBuilding(WorkHasBuilding $workHasBuilding): self
    {
        if ($this->workHasBuildings->contains($workHasBuilding)) {
            $this->workHasBuildings->removeElement($workHasBuilding);
            // set the owning side to null (unless already changed)
            if ($workHasBuilding->getBuilding() === $this) {
                $workHasBuilding->setBuilding(null);
            }
        }

        return $this;
    }

    /**
     * @return CoOwnership
     */

    public function getCoOwnership(): ?CoOwnership
    {
        return $this->coOwnership;
    }

    public function setCoOwnership(?CoOwnership $coOwnership): self
    {
        $this->coOwnership = $coOwnership;

        return $this;
    }

    public function __toString() {
      return $this->name;
    }

}
