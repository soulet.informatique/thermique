<?php

namespace App\Entity\CoOwnerShip;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Export\Export;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LotRepository")
 */
class Lot
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Type(type="string", message="La valeur {{ value }} n'est pas de type {{ type }}.")
     * @Assert\NotBlank(message="Cette valeur est obligatoire")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $number;

    /**
     * @Assert\Type(type="string", message="La valeur {{ value }} n'est pas de type {{ type }}.")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $staircase;

    /**
     * @Assert\Type(type="integer", message="La valeur {{ value }} n'est pas de type {{ type }}.")
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $floor;

    /**
     * @Assert\Type(type="string", message="La valeur {{ value }} n'est pas de type {{ type }}.")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $door;

    /**
     * @Assert\Type(type="integer", message="La valeur {{ value }} n'est pas de type {{ type }}.")
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"grant"})
     */
    private $apartmentNumber;


    /**
     * @Assert\Type(type="integer", message="La valeur {{ value }} n'est pas de type {{ type }}.")
     * 
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"grant"})
     */
    private $yearEntry;

    /**
     * @Assert\Type(type="boolean", message="La valeur {{ value }} n'est pas de type {{ type }}.")
     *
     * @ORM\Column(type="boolean")
     */
    private $mainResidence;

    /**
     * @Assert\Type(type="string", message="La valeur {{ value }} n'est pas de type {{ type }}.")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apartmentType;

    /**
     * @Assert\Type(type="integer", message="La valeur {{ value }} n'est pas de type {{ type }}.")
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"grant"})
     */
    private $roomNumber;

    /**
     * @Assert\Type(type="integer", message="La valeur {{ value }} n'est pas de type {{ type }}.")
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"grant"})
     */
    private $roomArea;

    /**
     * @Assert\Type(type="integer", message="La valeur {{ value }} n'est pas valide {{ type }}.")
     * @Assert\GreaterThanOrEqual(0)
     *
     * @ORM\Column(type="integer")
     * @Groups({"grant"})
     */
    private $tantiemesApartment;

    /**
     * @Assert\Type(type="integer", message="La valeur {{ value }} n'est pas valide {{ type }}.")
     * @Assert\GreaterThanOrEqual(0)
     *
     * @ORM\Column(type="integer")
     */
    private $tantiemesBox;

    /**
     * @Assert\Type(type="integer", message="La valeur {{ value }} n'est pas valide {{ type }}.")
     * @Assert\GreaterThanOrEqual(0)
     *
     * @ORM\Column(type="integer")
     */
    private $tantiemesCellar;

    /**
     * @Assert\Type(type="integer", message="La valeur {{ value }} n'est pas de type {{ type }}.")
     * @Assert\GreaterThanOrEqual(0)
     *
     * @ORM\Column(type="integer")
     */
    private $tantiemesCommon;

    /**
     * @Assert\Type(type="integer", message="La valeur {{ value }} n'est pas de type {{ type }}.")
     * @Assert\GreaterThanOrEqual(0)
     *
     * @ORM\Column(type="integer")
     */
    private $tantiemesShop;

    /**
     * @Assert\Type(type="integer", message="La valeur {{ value }} n'est pas valide {{ type }}.")
     * @Assert\GreaterThanOrEqual(0)
     *
     * @ORM\Column(type="integer")
     */
    private $tantiemesHeating;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\CoOwnerShip\Owner", mappedBy="lots", cascade={"persist", "remove"})
     */
    private $owners;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CoOwnerShip\Building", inversedBy="lots")
     */
    private $building;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Export\Export", mappedBy="lot")
     */
    private $exports;

    public function __construct()
    {
        $this->exports = new ArrayCollection();
        $this->owners = new ArrayCollection();
        $this->setMainResidence(false);
        $this->setApartmentNumber(1);
        $this->setRoomArea(0);
        $this->setRoomNumber(1);
        $this->setTantiemesApartment(0);
        $this->setTantiemesBox(0);
        $this->setTantiemesCellar(0);
        $this->setTantiemesCommon(0);
        $this->setTantiemesHeating(0);
        $this->setTantiemesShop(0);
    }

    public function __toString() {
        return sprintf("%s %s", $this->getBuilding(), $this->getNumber());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getStaircase(): ?string
    {
        return $this->staircase;
    }

    public function setStaircase(string $staircase): self
    {
        $this->staircase = $staircase;

        return $this;
    }

    public function getFloor(): ?int
    {
        return $this->floor;
    }

    public function setFloor(int $floor): self
    {
        $this->floor = $floor;

        return $this;
    }

    public function getDoor(): ?string
    {
        return $this->door;
    }

    public function setDoor(string $door): self
    {
        $this->door = $door;

        return $this;
    }

    public function getLivingSpace(): ?float
    {
        return $this->livingSpace;
    }

    public function setLivingSpace(float $livingSpace): self
    {
        $this->livingSpace = $livingSpace;

        return $this;
    }

    public function getApartmentNumber(): ?int
    {
        return $this->apartmentNumber;
    }

    public function setApartmentNumber(int $apartmentNumber): self
    {
        $this->apartmentNumber = $apartmentNumber;

        return $this;
    }

    public function getYearEntry(): ?int
    {
        return $this->yearEntry;
    }

    public function setYearEntry(int $yearEntry): self
    {
        $this->yearEntry = $yearEntry;

        return $this;
    }

    public function getMainResidence(): ?bool
    {
        return $this->mainResidence;
    }

    public function setMainResidence(bool $mainResidence): self
    {
        $this->mainResidence = $mainResidence;

        return $this;
    }

    public function getApartmentType(): ?string
    {
        return $this->apartmentType;
    }

    public function setApartmentType(string $apartmentType): self
    {
        $this->apartmentType = $apartmentType;

        return $this;
    }

    public function getRoomNumber(): ?int
    {
        return $this->roomNumber;
    }

    public function setRoomNumber(int $roomNumber): self
    {
        $this->roomNumber = $roomNumber;

        return $this;
    }

    public function getRoomArea(): ?int
    {
        return $this->roomArea;
    }

    public function setRoomArea(int $roomArea): self
    {
        $this->roomArea = $roomArea;

        return $this;
    }

    public function getTantiemesApartment(): ?int
    {
        return $this->tantiemesApartment;
    }

    public function setTantiemesApartment(int $tantiemesApartment): self
    {
        $this->tantiemesApartment = $tantiemesApartment;

        return $this;
    }

    public function getTantiemesBox(): ?int
    {
        return $this->tantiemesBox;
    }

    public function setTantiemesBox(int $tantiemesBox): self
    {
        $this->tantiemesBox = $tantiemesBox;

        return $this;
    }

    public function getTantiemesCellar(): ?int
    {
        return $this->tantiemesCellar;
    }

    public function setTantiemesCellar(int $tantiemesCellar): self
    {
        $this->tantiemesCellar = $tantiemesCellar;

        return $this;
    }

    public function getTantiemesHeating(): ?int
    {
        return $this->tantiemesHeating;
    }

    public function setTantiemesHeating(int $tantiemesHeating): self
    {
        $this->tantiemesHeating = $tantiemesHeating;

        return $this;
    }


    public function getTantiemesCommon(): ?int
    {
        return $this->tantiemesCommon;
    }

    public function setTantiemesCommon(int $tantiemesCommon): self
    {
        $this->tantiemesCommon = $tantiemesCommon;

        return $this;
    }

    public function getTantiemesShop(): ?int
    {
        return $this->tantiemesShop;
    }

    public function setTantiemesShop(int $tantiemesShop): self
    {
        $this->tantiemesShop = $tantiemesShop;

        return $this;
    }

    public function getBuilding(): ?Building
    {
        return $this->building;
    }

    public function setBuilding(?Building $building): self
    {
        $this->building = $building;

        return $this;
    }

    /**
     * @return Collection|Export[]
     */
    public function getExports(): Collection
    {
        return $this->exports;
    }

    public function addExport(Export $export): self
    {
        if (!$this->exports->contains($export)) {
            $this->exports[] = $export;
            $export->setLot($this);
        }

        return $this;
    }

    public function removeExport(Export $export): self
    {
        if ($this->exports->contains($export)) {
            $this->exports->removeElement($export);
            // set the owning side to null (unless already changed)
            if ($export->getLot() === $this) {
                $export->setLot(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Owner[]
     */
    public function getOwners(): Collection
    {
        return $this->owners;
    }

    public function getFirstOwner(): Owner
    {
        $owner = null;
        foreach($this->owners as $elt) {
            $owner = $elt;
            break;
        }
        return $owner;
    }

    public function addOwner(Owner $owner): self
    {
        if (!$this->owners->contains($owner)) {
            $this->owners[] = $owner;
            $owner->addLot($this);
        }

        return $this;
    }

    public function removeOwner(Owner $owner): self
    {
        if ($this->owners->contains($owner)) {
            $this->owners->removeElement($owner);
            // set the owning side to null (unless already changed)
            if ($owner->getLots()->contains($this)) {
                $owner->removeLot($this);
            }
        }

        return $this;
    }
}
