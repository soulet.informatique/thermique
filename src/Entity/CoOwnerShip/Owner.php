<?php

namespace App\Entity\CoOwnerShip;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;
use App\Entity\User\User;
use App\Entity\Scenario\Scenario;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OwnerRepository")
 */
class Owner
{

    const HOUSING_USE = 'occupant';
    const HOUSING_LESSOR = 'bailleur';
    const HOUSING_RENT = 'locataire';
    const HOUSING_FREE = 'gratuit';


    public static $housingsChoices = array(
        'owner.housing_use' => self::HOUSING_USE,
        'owner.housing_lessor' => self::HOUSING_LESSOR,
        'owner.housing_rent' => self::HOUSING_RENT,
        'owner.housing_free' => self::HOUSING_FREE,
    );

    public static $housingChoices = array(
        self::HOUSING_USE,
        self::HOUSING_LESSOR,
        self::HOUSING_RENT,
        self::HOUSING_FREE,
    );

    const MARITAL_SINGLE = 'personne_seule';
    const MARITAL_COUPLE = 'couple';
    const MARITAL_PARENTS = 'parents';
    const MARITAL_SINGLE_PARENT = 'parent_seul';

    public static $maritalsChoices = array(
        'owner.marital_single' => self::MARITAL_SINGLE,
        'owner.marital_couple' => self::MARITAL_COUPLE,
        'owner.marital_parents' => self::MARITAL_PARENTS,
        'owner.marital_single_parent' => self::MARITAL_SINGLE_PARENT,
    );

    public static $maritalChoices = array(
        self::MARITAL_SINGLE,
        self::MARITAL_COUPLE,
        self::MARITAL_PARENTS,
        self::MARITAL_SINGLE_PARENT,
    );
    
    const GENDER_MME = 'Mme';
    const GENDER_M = 'M.';
    const GENDER_MME_OR_M = 'Mme ou M.';
    const GENDER_MS = 'Ms';
    const GENDER_MMES = 'Mmes';

    public static $gendersChoices = array(
        'owner.gender_mme' => self::GENDER_MME,
        'owner.gender_m' => self::GENDER_M,
        'owner.gender_mme_or_m' => self::GENDER_MME_OR_M,
        'owner.gender_mmes' => self::GENDER_MMES,
        'owner.gender_ms' => self::GENDER_MS,
    );

    const ALERT_PERSONAL_DATA = 'alert_personal_data';
    const ALERT_REVENUE_ONE = 'alert_revenue_one';
    const ALERT_REVENUE_TWO = 'alert_revenue_two';
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gender;

    /**
     * @Assert\Type("string")
     * )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank(message="Le champ { value } est obligatoire")
     * )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank(message="Le champ { value } est obligatoire")
     *
     * @ORM\Column(type="string", length=255)
     */
    private $postalCode;

    /**
     * @Assert\Type("string")
     * 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address1;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank(message="Le champ { value } est obligatoire")
     * 
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone1;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone2;

    /**
     * @Assert\Type("string")
     * @Assert\Email(checkMX = true, message = "L'adresse mail '{{ value }}' n'est pas valide."
     * )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mail;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"grant"})
     */
    private $familyMemberNumber;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     * @Assert\GreaterThanOrEqual(0)
     * 
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"grant"})
     */
    private $childrenNumber;

    /**
     * @Assert\Type("string", groups="step2")
     * @Assert\Choice(callback={"App\Entity\CoOwnerShip\Owner","getMaritalChoices"}, groups={"step2"})
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"grant"})
     */
    private $maritalStatus;

    /**
     * @Assert\Type("boolean")
     *
     * @ORM\Column(type="boolean")
     * @Groups({"grant"})
     */
    private $retired;

    /**
     * @Assert\Type("string")
     * @Assert\Choice(callback={"App\Entity\CoOwnerShip\Owner","getHousingChoices"}, groups={"step2"})
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"grant"})
     */
    private $housingSituation;

    /**
     * @Assert\Regex(
     *     groups="step2",
     *     pattern="/^[0-9]*$/",
     *     message="La valeur {{ value }} n'est pas un entier."
     * )
     *
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"grant"})
     */
    private $taxRevenueOne;

    /**
     * @Assert\Regex(
     *     groups="step2",
     *     pattern="/^[0-9]*$/",
     *     message="La valeur {{ value }} n'est pas un entier."
     * )
     *
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"grant"})
     */
    private $taxRevenueTwo;

    /**
     * @Assert\Type("boolean")
     *
     * @ORM\Column(type="boolean")
     * @Groups({"grant"})
     */
    private $gotAnah;

    /**
     * @Assert\Type(
     *     type="float",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"grant"})
     */
    private $subAnahAmount;

    /**
     * @Assert\Type("boolean")
     *
     * @ORM\Column(type="boolean")
     * @Groups({"grant"})
     */
    private $gotTaxCredit;

    /**
     * @Assert\Type(
     *     type="float",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"grant"})
     */
    private $taxCreditAmount;

    /**
     * @Assert\Type("boolean")
     *
     * @ORM\Column(type="boolean")
     * @Groups({"grant"})
     */
    private $gotEcoCredit;

    /**
     * @Assert\Type(
     *     type="float",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"grant"})
     */
    private $ecoCreditAmount;

    /**
     * @Assert\Type("boolean")
     *
     * @ORM\Column(type="boolean")
     * @Groups({"grant"})
     */
    private $gotPTZ;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $step;

    /**
     * @Assert\Type("boolean")
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $personalData;

    /**
     * @Assert\Type("boolean")
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $paperCopy;

    /**
     * @Assert\Type("boolean")
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $manualForm;

    /**
     * 
     * @ORM\ManyToMany(targetEntity="App\Entity\CoOwnerShip\Lot", inversedBy="owners", cascade={"persist", "remove"})
     */
    private $lots;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User\User", mappedBy="owner", cascade={"remove"})
     */
    private $user;

    /**
     * Used to store attachedFiles
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Document\UploadedFile", mappedBy="owner", cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $attachedFiles;

    public function __construct()
    {
        $this->lots = new ArrayCollection();
        $this->setRetired(false);
        $this->setGotAnah(false);
        $this->setGotTaxCredit(false);
        $this->setGotEcoCredit(false);
        $this->setGotPTZ(false);
        $this->setCompany('');
        $this->setChildrenNumber(0);
        $this->setFamilyMemberNumber(0);
        $this->setStep(0);
        $this->attachedFiles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public static function getMaritalChoices()
    {
        return self::$maritalChoices;
    }

    public static function getHousingChoices()
    {
        return self::$housingChoices;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInvestment(Scenario $scenario): ?int
    {
        $lot = $this->getFirstLot();
        $total = $scenario->getInvestismentTtcByBuilding($lot->getBuilding());
        return floor($total * $lot->getTantiemesApartment() / $lot->getBuilding()->getTantiemes());
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender = null): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname = null): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getAddressName(): ?string
    {
        if($this->getGender()) {
            return sprintf("%s %s %s", $this->getGender(), $this->getLastname(), $this->getFirstname() ? $this->getFirstname() : "");
        } else {
            return sprintf("%s %s %s", $this->getCompany() ? $this->getCompany() : "", $this->getLastname(), $this->getFirstname() ? $this->getFirstname() : "");
        }
    }

    public function getGlobalAddress(): ?string
    {
        $officeLine = '</w:t></w:r></w:p><w:p><w:pPr><w:pStyle w:val="Normal"/><w:tabs><w:tab w:val="left" w:pos="5245" w:leader="none"/></w:tabs><w:ind w:left="5387" w:hanging="0"/><w:jc w:val="both"/><w:rPr><w:lang w:eastAsia="en-GB"/></w:rPr></w:pPr><w:r><w:rPr><w:lang w:eastAsia="en-GB"/></w:rPr><w:t>';
        $result = '';
        if($this->getAddress1()) {
            $result .= sprintf("%s%s", $this->getAddress1(), $officeLine);
        }
        if($this->getAddress2()) {
            $result .= sprintf("%s%s", $this->getAddress2(), $officeLine);
        }
        $result .= sprintf("%s %s", $this->getPostalCode(), $this->getCity());
        if($this->getCountry() and $this->getCountry() != 'France') {
            $result .= sprintf("%s%s", $officeLine, $this->getCountry());
        }
        return $result;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function setAddress1(string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(string $address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPhone1(): ?string
    {
        return $this->phone1;
    }

    public function setPhone1(string $phone1): self
    {
        $this->phone1 = $phone1;

        return $this;
    }

    public function getPhone2(): ?string
    {
        return $this->phone2;
    }

    public function setPhone2(string $phone2): self
    {
        $this->phone2 = $phone2;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    protected function getAdultsNumber()
    {
        switch($this->getMaritalStatus()) {
            case self::MARITAL_SINGLE:
            case self::MARITAL_SINGLE_PARENT:
                return 1;
            case self::MARITAL_PARENTS:
            case self::MARITAL_COUPLE:
                return 2;
        }
        return 0;
    }

    protected function updateFamilyMemberNumber()
    {
        $children = 0;
        switch($this->getMaritalStatus()) {
            case self::MARITAL_SINGLE_PARENT:
            case self::MARITAL_PARENTS:
                $children = $this->getChildrenNumber();
                break;
        }
        $this->setFamilyMemberNumber($this->getAdultsNumber() + $children);
    }

    public function getFamilyMemberNumber(): ?int
    {
        return $this->familyMemberNumber;
    }

    public function setFamilyMemberNumber(int $familyMemberNumber): self
    {
        $this->familyMemberNumber = $familyMemberNumber;

        return $this;
    }

    public function getChildrenNumber(): ?int
    {
        return $this->childrenNumber;
    }

    public function setChildrenNumber(int $childrenNumber): self
    {
        $this->childrenNumber = $childrenNumber;
        $this->updateFamilyMemberNumber();

        return $this;
    }

    public function getStep(): ?int
    {
        return $this->step;
    }

    public function isForm(): ?int
    {
        return count($this->getAlerts()) == 0 and $this->getStep() == 3;
    }

    public function setStep(int $step): self
    {
        $this->step = $step;

        return $this;
    }

    public function setMaxStep(int $step): self
    {
        if($step > $this->step) {
            $this->setStep($step);
        }

        return $this;
    }

    public function getMaritalStatus(): ?string
    {
        return $this->maritalStatus;
    }

    public function setMaritalStatus(string $maritalStatus): self
    {
        $this->maritalStatus = $maritalStatus;
        $this->updateFamilyMemberNumber();

        return $this;
    }

    public function getRetired(): ?bool
    {
        return $this->retired;
    }

    public function setRetired(bool $retired): self
    {
        $this->retired = $retired;

        return $this;
    }

    public function getHousingSituation(): ?string
    {
        return $this->housingSituation;
    }

    public function setHousingSituation(string $housingSituation): self
    {
        $this->housingSituation = $housingSituation;

        return $this;
    }

    public function getTaxRevenueOne(): ?string
    {
        return $this->taxRevenueOne;
    }

    public function setTaxRevenueOne(string $taxRevenueOne = null): self
    {
        $this->taxRevenueOne = $taxRevenueOne;

        return $this;
    }

    public function getTaxRevenueTwo(): ?string
    {
        return $this->taxRevenueTwo;
    }

    public function setTaxRevenueTwo(string $taxRevenueTwo = null): self
    {
        $this->taxRevenueTwo = $taxRevenueTwo;

        return $this;
    }

    protected function testAlert($testMaxStep, $alertStep)
    {
        // alert corresponds to a step (ex: personal data on step 1)
        if($testMaxStep and $this->getStep() < $alertStep) {
            return false;
        }
        return true;
    }

    public function getAlerts($testMaxStep = true)
    {
        $alerts = array();

        // personal alert
        if($this->testAlert($testMaxStep, 1)) {
            if(!$this->getPersonalData()) {
                $alerts[] = self::ALERT_PERSONAL_DATA;
            }
        }

        // personal alert
        if($this->testAlert($testMaxStep, 2)) {
            if($this->getTaxRevenueOne() === null) {
                $alerts[] = self::ALERT_REVENUE_ONE;
            }
            if($this->getAdultsNumber() > 1 and
                $this->getTaxRevenueTwo() === null) {
                $alerts[] = self::ALERT_REVENUE_TWO;
            }
        }
        return $alerts;
    }

    public function getGotAnah(): ?bool
    {
        return $this->gotAnah;
    }

    public function setGotAnah(bool $gotAnah): self
    {
        $this->gotAnah = $gotAnah;

        return $this;
    }

    public function getPersonalData(): ?bool
    {
        return $this->personalData;
    }

    public function setPersonalData(bool $personalData): self
    {
        $this->personalData = $personalData;

        return $this;
    }

    public function getPaperCopy(): ?bool
    {
        return $this->paperCopy;
    }

    public function setPaperCopy(bool $paperCopy): self
    {
        $this->paperCopy = $paperCopy;

        return $this;
    }

    public function getManualForm(): ?bool
    {
        return $this->manualForm;
    }

    public function setManualForm(bool $manualForm): self
    {
        $this->manualForm = $manualForm;
        if($manualForm) {
            $this->setStep(3);
        }

        return $this;
    }

    public function getSubAnahAmount(): ?float
    {
        return $this->subAnahAmount;
    }

    public function setSubAnahAmount(float $subAnahAmount): self
    {
        $this->subAnahAmount = $subAnahAmount;

        return $this;
    }

    public function getGotTaxCredit(): ?bool
    {
        return $this->gotTaxCredit;
    }

    public function setGotTaxCredit(bool $gotTaxCredit): self
    {
        $this->gotTaxCredit = $gotTaxCredit;

        return $this;
    }

    public function getTaxCreditAmount(): ?float
    {
        return $this->taxCreditAmount;
    }

    public function setTaxCreditAmount(float $taxCreditAmount): self
    {
        $this->taxCreditAmount = $taxCreditAmount;

        return $this;
    }

    public function getGotEcoCredit(): ?bool
    {
        return $this->gotEcoCredit;
    }

    public function setGotEcoCredit(bool $gotEcoCredit): self
    {
        $this->gotEcoCredit = $gotEcoCredit;

        return $this;
    }

    public function getEcoCreditAmount(): ?float
    {
        return $this->ecoCreditAmount;
    }

    public function setEcoCreditAmount(float $ecoCreditAmount): self
    {
        $this->ecoCreditAmount = $ecoCreditAmount;

        return $this;
    }

    public function getGotPTZ(): ?bool
    {
        return $this->gotPTZ;
    }

    public function setGotPTZ(bool $gotPTZ): self
    {
        $this->gotPTZ = $gotPTZ;

        return $this;
    }

    /**
     * @return Collection|Lot[]
     */
    public function getLots(): Collection
    {
        return $this->lots;
    }

    public function getFirstLot(): Lot
    {
        $lot = null;
        foreach($this->lots as $elt) {
            $lot = $elt;
            break;
        }
        return $lot;
    }

    public function getCoOwnership()
    {
        return $this->getFirstLot()->getBuilding()->getCoOwnership();
    }

    public function addLot(Lot $lot): self
    {
        if (!$this->lots->contains($lot)) {
            $this->lots[] = $lot;
            $lot->addOwner($this);
        }

        return $this;
    }

    public function removeLot(Lot $lot): self
    {
        if ($this->lots->contains($lot)) {
            $this->lots->removeElement($lot);
            // set the owning side to null (unless already changed)
            if ($lot->getOwners()->contains($this)) {
                $lot->removeOwner($this);
            }
        }

        return $this;
    }

    public function __toString() {
      return $this->getAddressName();
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Add attachedFile
     *
     * @param \App\Entity\Document\UploadedFile $attachedFile
     *
     * @return Owner
     */
    public function addAttachedFile(\App\Entity\Document\UploadedFile $attachedFile)
    {
        $this->attachedFiles[] = $attachedFile;
        return $this;
    }

    /**
     * Remove attachedFile
     *
     * @param \App\Entity\Document\UploadedFile $attachedFile
     */
    public function removeAttachedFile(\App\Entity\Document\UploadedFile $attachedFile)
    {
        $this->attachedFiles->removeElement($attachedFile);
    }

    /**
     * Get attachedFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachedFiles()
    {
        $attachedFiles = $this->attachedFiles->toArray();;
        usort($attachedFiles, function($a, $b) {
            return ($a->getLastUpdate() > $b->getLastUpdate()) ? -1 : 1;
        });
        return $attachedFiles;
    }
}
