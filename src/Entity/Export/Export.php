<?php

namespace App\Entity\Export;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Grant\Grant;
use App\Entity\CoOwnerShip\Lot;
use App\Entity\Scenario\Scenario;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExportRepository")
 */
class Export
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Grant\Grant", inversedBy="exports")
     */
    private $grant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CoOwnerShip\Lot", inversedBy="exports")
     */
    private $lot;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Scenario\Scenario", inversedBy="exports")
     */
    private $scenario;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGrant(): ?Grant
    {
        return $this->grant;
    }

    public function setGrant(?Grant $grant): self
    {
        $this->grant = $grant;

        return $this;
    }

    public function getLot(): ?Lot
    {
        return $this->lot;
    }

    public function setLot(?Lot $lot): self
    {
        $this->lot = $lot;

        return $this;
    }

    public function getScenario(): ?Scenario
    {
        return $this->scenario;
    }

    public function setScenario(?Scenario $scenario): self
    {
        $this->scenario = $scenario;

        return $this;
    }
}
