<?php

namespace App\Entity\Scenario;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\CoOwnerShip\Building;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WorkHasBuildingRepository")
 */
class WorkHasBuilding
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Scenario\Work", inversedBy="workHasBuildings")
     */
    private $work;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CoOwnerShip\Building", inversedBy="workHasBuildings")
     */
    private $building;

    /**
     * @Assert\Regex("/^\d+(\.\d+)?/")
     *
     * @ORM\Column(type="decimal", precision=9, scale=2)
     * @Groups({"grant"})
     */
    private $costHt;

    /**
     * @Assert\Regex("/^\d+(\.\d+)?/")
     *
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $costTtc;

    /**
     * @Assert\Type(
     *     type="float",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="float")
     */
    private $syndicFees;

    /**
     * @Assert\Type(
     *     type="float",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="float")
     */
    private $masteryFees;

    /**
     * @Assert\Type(
     *     type="float",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="float")
     */
    private $spsFees;

    /**
     * @Assert\Type(
     *     type="float",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="float")
     */
    private $technicalFees;

    /**
     * @Assert\Type(
     *     type="float",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="float")
     */
    private $insurance;

    /**
     * @Assert\Regex("/^\d+(\.\d+)?/")
     *
     * @ORM\Column(type="decimal", precision=9, scale=2)
     * @Groups({"grant"})
     */
    private $cee;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} n'est pas un nombre."
     * )
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"grant"})
     */
    private $caracValue;

    public function __construct()
    {
        $this->setCostHt(0);
        $this->setCostTtc(0);
        $this->setCee(0);
    }

    public function __toString() {
        return sprintf('%s - %s', $this->work, $this->building);
    }


    /**
     * @VirtualProperty
     * @SerializedName("carac_type")
     * @Groups({"grant"}))
     */
    public function getCaractType() {
        if($this->work) {
            return $this->work->getCaracType();
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWork(): ?Work
    {
        return $this->work;
    }

    public function setWork(?Work $work): self
    {
        $this->work = $work;

        return $this;
    }

    public function getBuilding(): ?Building
    {
        return $this->building;
    }

    public function setBuilding(?Building $building): self
    {
        $this->building = $building;

        return $this;
    }

    public function getCostHt(): ?string
    {
        return $this->costHt;
    }

    public function setCostHt(string $costHt): self
    {
        $this->costHt = $costHt;

        return $this;
    }

    public function getCostTtc(): ?string
    {
        return $this->costTtc;
    }

    public function getTotalCostTtc(): ?float
    {
        return $this->costTtc + $this->syndicFees + $this->masteryFees + $this->spsFees + $this->technicalFees + $this->insurance;
    }

    public function setCostTtc(string $costTtc): self
    {
        $this->costTtc = $costTtc;

        return $this;
    }

    public function getSyndicFees(): ?float
    {
        return $this->syndicFees;
    }

    public function setSyndicFees(float $syndicFees): self
    {
        $this->syndicFees = $syndicFees;

        return $this;
    }

    public function getMasteryFees(): ?float
    {
        return $this->masteryFees;
    }

    public function setMasteryFees(float $masteryFees): self
    {
        $this->masteryFees = $masteryFees;

        return $this;
    }

    public function getSpsFees(): ?float
    {
        return $this->spsFees;
    }

    public function setSpsFees(float $spsFees): self
    {
        $this->spsFees = $spsFees;

        return $this;
    }

    public function getTechnicalFees(): ?float
    {
        return $this->technicalFees;
    }

    public function setTechnicalFees(float $technicalFees): self
    {
        $this->technicalFees = $technicalFees;

        return $this;
    }

    public function getInsurance(): ?float
    {
        return $this->insurance;
    }

    public function setInsurance(float $insurance): self
    {
        $this->insurance = $insurance;

        return $this;
    }
    public function getCee(): ?string
    {
        return $this->cee;
    }

    public function setCee(string $cee): self
    {
        $this->cee = $cee;

        return $this;
    }

    public function getCaracValue(): ?int
    {
        return $this->caracValue;
    }

    public function setCaracValue(int $caracValue): self
    {
        $this->caracValue = $caracValue;

        return $this;
    }
}
