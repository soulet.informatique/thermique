<?php

namespace App\Entity\Scenario;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WorkHasScenarioRepository")
 */
class WorkHasScenario
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Scenario\Work", inversedBy="workHasScenarios")
     */
    private $work;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Scenario\Scenario", inversedBy="workHasScenarios")
     */
    private $scenario;

    public function __toString() {
        return sprintf('%s - %s', $this->work, $this->scenario);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWork(): ?Work
    {
        return $this->work;
    }

    public function setWork(?Work $work): self
    {
        $this->work = $work;

        return $this;
    }

    public function getScenario(): ?Scenario
    {
        return $this->scenario;
    }

    public function setScenario(?Scenario $scenario): self
    {
        $this->scenario = $scenario;

        return $this;
    }
}
