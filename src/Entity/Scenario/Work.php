<?php

namespace App\Entity\Scenario;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Scenario\WorkHasBuilding;
use App\Entity\CoOwnerShip\CoOwnership;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WorkRepository")
 */
class Work
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Assert\Type("string")
     *
     * @ORM\Column(type="string", length=255)
     */
    private $caracType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Scenario\WorkHasBuilding", mappedBy="work", cascade={"persist"})
     */
    private $workHasBuildings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Scenario\WorkHasScenario", mappedBy="work")
     */
    private $workHasScenarios;

    public function __construct()
    {
        $this->workHasBuildings = new ArrayCollection();
        $this->workHasScenarios = new ArrayCollection();
    }

    public function __toString() {

        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCaracType(): ?string
    {
        return $this->caracType;
    }

    public function setCaracType(string $caracType): self
    {
        $this->caracType = $caracType;

        return $this;
    }

    /**
     * @return Collection|WorkHasBuilding[]
     */
    public function getWorkHasBuildings(): Collection
    {
        return $this->workHasBuildings;
    }

    public function addWorkHasBuilding(WorkHasBuilding $workHasBuilding): self
    {
        if (!$this->workHasBuildings->contains($workHasBuilding)) {
            $this->workHasBuildings[] = $workHasBuilding;
            $workHasBuilding->setWorks($this);
        }

        return $this;
    }

    public function removeWorkHasBuilding(WorkHasBuilding $workHasBuilding): self
    {
        if ($this->workHasBuildings->contains($workHasBuilding)) {
            $this->workHasBuildings->removeElement($workHasBuilding);
            // set the owning side to null (unless already changed)
            if ($workHasBuilding->getWorks() === $this) {
                $workHasBuilding->setWorks(null);
            }
        }

        return $this;
    }

    public function getCoOwnership() : CoOwnership
    {
        foreach($this->getWorkHasBuildings() as $whb) {
            return $whb->getBuilding()->getCoOwnership();
        }
        return null;
    }

    /**
     * @return Collection|WorkHasScenario[]
     */
    public function getWorkHasScenarios(): Collection
    {
        return $this->workHasScenarios;
    }

    public function addWorkHasScenario(WorkHasScenario $workHasScenario): self
    {
        if (!$this->workHasScenarios->contains($workHasScenario)) {
            $this->workHasScenarios[] = $workHasScenario;
            $workHasScenario->setWorks($this);
        }

        return $this;
    }

    public function removeWorkHasScenario(WorkHasScenario $workHasScenario): self
    {
        if ($this->workHasScenarios->contains($workHasScenario)) {
            $this->workHasScenarios->removeElement($workHasScenario);
            // set the owning side to null (unless already changed)
            if ($workHasScenario->getWorks() === $this) {
                $workHasScenario->setWorks(null);
            }
        }

        return $this;
    }
}
