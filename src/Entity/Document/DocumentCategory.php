<?php
namespace App\Entity\Document;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\Repository\Document\ParameterRepository")
 */
class DocumentCategory
{
    const DOC_TYPE_TEMPLATE = 'template';
    const DOC_TYPE_DOCUMENT = 'owner_file';

    public static $categoriesChoices = array(
        'documentCategory.template' => self::DOC_TYPE_TEMPLATE,
        'documentCategory.owner' => self::DOC_TYPE_DOCUMENT,
    );

    const POPULATION_TYPE_FORM = 'form';
    const POPULATION_TYPE_NO_FORM = 'no_form';
    const POPULATION_TYPE_ALL = 'all';

    public static $populationsChoices = array(
        'documentCategory.form' => self::POPULATION_TYPE_FORM,
        'documentCategory.noForm' => self::POPULATION_TYPE_NO_FORM,
        'documentCategory.all' => self::POPULATION_TYPE_ALL,
    );
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull()
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull()
     *
     * @var string
     */
    private $setting;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $population;

    /**
     * @ORM\ManyToOne(targetEntity="DocumentCategory")
     */
    private $childCategory;

    public function __toString()
    {
        return  $this->name ;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DocumentCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set setting
     *
     * @param string $setting
     *
     * @return DocumentCategory
     */
    public function setSetting($setting)
    {
        $this->setting = $setting;

        return $this;
    }

    /**
     * Get setting
     *
     * @return string
     */
    public function getSetting()
    {
        return $this->setting;
    }

    /**
     * Set population
     *
     * @param string $population
     *
     * @return DocumentCategory
     */
    public function setPopulation($population)
    {
        $this->population = $population;

        return $this;
    }

    /**
     * Get population
     *
     * @return string
     */
    public function getPopulation()
    {
        return $this->population;
    }
    
    /**
     * Set childCategory
     *
     * @param \App\Entity\Document\DocumentCategory $documentCategory
     *
     * @return DocumentCategory
     */
    public function setChildCategory(\App\Entity\Document\DocumentCategory $documentCategory = null)
    {
        $this->childCategory = $documentCategory;

        return $this;
    }

    /**
     * Get childCategory
     *
     * @return \App\Entity\Document\DocumentCategory
     */
    public function getChildCategory()
    {
        return $this->childCategory;
    }
}
