<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Scenario\Scenario;
use Faker;

class ScenarioFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        for ($i = 1; $i <= 10; $i++)
         {
            $scenario = new Scenario();
            $scenario
                ->setName($faker->name)
                ->setEnergyExpens($faker->numberBetween($min = 0, $max = 10000))
                ->setMaintenanceExpens($faker->numberBetween($min = 0, $max = 100000))
                ->setLargeExpens($faker->randomNumber)
                ->setCep($faker->numberBetween($min = 0, $max = 100000))
                ->setCarbonIndex($faker->randomDigitNotNull)
                ->setCarbonIndexTag($faker->randomDigitNotNull)
                ->setConfort($faker->randomDigitNotNull)
                ->setInheritanceGain($faker->numberBetween($min = 0, $max = 100))
                ->setEnergyFinalTag($faker->randomLetter)
                ->setEnergyGain($faker->numberBetween($min = 0, $max = 100))
                ->setEnvironmentalGain($faker->numberBetween($min = 0, $max = 100))
                ->setEnvironmentalGainTag($faker->randomLetter)
                ->setEnvironmentalGainTotal($faker->numberBetween($min = 0, $max = 100000))
                ->setLabelGain($faker->randomLetter)
                ->setTimeReturn($faker->numberBetween($min = 0, $max = 100))
                ->setInvestismentTtc($faker->numberBetween($min = 0, $max = 100000))
                ->setInvestismentHt($faker->randomDigit)
                ->setLoadsEnergyGain($faker->numberBetween($min = 0, $max = 100))
                ->setLoadsGain($faker->numberBetween($min = 0, $max = 100))

;



        $manager->persist($scenario);
        }
        $manager->flush();
    }
}