<?php

namespace App\Form;

use App\Entity\Scenario\Work;
use App\Entity\Scenario\WorkHasBuilding;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Form\WorkHasBuildingType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class WorkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('name', TextType::class)
            ->add('caracType', TextType::class, [
                'required' => false,
            ])
          ->add('workHasBuildings', CollectionType::class, [
            'entry_type' => WorkHasBuildingType::class,
            'label' => false,
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => true,
            'entry_options' => [
              'coOwnership' => $options['coOwnership'],
            ]
          ])
          ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['coOwnership']);
        $resolver->setDefaults([
            'data_class' => Work::class,
        ]);
    }
}
