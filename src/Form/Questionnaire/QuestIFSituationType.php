<?php

namespace App\Form\Questionnaire;

use App\Entity\CoOwnerShip\Owner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class QuestIFSituationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('maritalStatus', ChoiceType::class, [
                'choices' => Owner::$maritalsChoices,
                'attr' => [
                    'placeholder' => 'Choisissez une option'],
                'label' => 'Statut marital'
            ])
            ->add('retired', CheckboxType::class, [
                'required' => false,
                'label' => 'Cochez cette case si vous êtes retraité',
//            'attr' => [
//                'class' => 'switch_1'
//            ]
            ])
        ->add('childrenNumber', IntegerType::class, [
            'required' => true,
            'empty_data' => '0',
            'attr' => [
                'placeholder' => 'ex : 2'
            ],
            'label' => 'Nombre d\'enfants'
        ])
        ->add('housingSituation', ChoiceType::class, [
            'choices' => Owner::$housingsChoices,
            'attr' => [
            'placeholder' => 'Choisissez une option'
            ],
            'label' => 'Situation'
        ] )
        ->add('taxRevenueOne', IntegerType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Exemple: 20527'
            ],
            'label' => 'owner.taxRevenueOne'
        ])
        ->add('taxRevenueTwo',NumberType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Exemple: 20527'
            ],
            'label' => 'owner.taxRevenueTwo'
        ])
            ->add('gotPTZ', CheckboxType::class,[
                'required' => false,
//                'attr' => [
//                    'class' => 'switch_1'
//                ],
                'label' => 'Avez-vous bénéficiez d\'un prêt à Taux Zéro ?'
            ])
        ->add('submit', SubmitType::class, array('label' => 'Suivant'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Owner::class,
        ]);
    }
}