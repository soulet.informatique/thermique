<?php

namespace App\Form;

use App\Entity\CoOwnerShip\Lot;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class LotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', TextType::class)
            ->add('staircase', TextType::class)
            ->add('floor', TextType::class)
            ->add('door', TextType::class)
            ->add('apartmentType', TextType::class)

            ->add('roomNumber', IntegerType::class)
            ->add('roomArea', IntegerType::class)
            ->add('mainResidence', CheckboxType::class, [
                'required' => false
            ])
            ->add('apartmentNumber', IntegerType::class)

            ->add('tantiemesApartment', IntegerType::class)
            ->add('tantiemesBox', IntegerType::class)
            ->add('tantiemesCellar', IntegerType::class)
            ->add('tantiemesCommon', IntegerType::class)
            ->add('tantiemesShop', IntegerType::class)
            ->add('tantiemesHeating', IntegerType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lot::class,
        ]);
    }
}
