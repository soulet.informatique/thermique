<?php

namespace App\Form;

use App\Entity\Export\Upload;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UploadType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $builder
        ->add('file', FileType::class, [
          'attr' => [
            'placeholder' => 'Choose File',
            'class' => 'file-upload'
          ]
        ])
        ->add('submit', SubmitType::class)
      ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults([
          'data_class' => Upload::class,
      ]);
  }

}
