<?php

namespace App\Form;

use App\Entity\Scenario\Scenario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Form\WorkHasScenarioType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Scenario\Work;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;

class ScenarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => "Nom"
            ])
            ->add('energyExpens', NumberType::class, [
                    'required' => false,
                    'empty_data' =>"0",
                'label' => "Dépense annuelle d'énergie"
            ])
            ->add('maintenanceExpens', NumberType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Dépense annuelle d'entretien courant"
            ])
            ->add('largeExpens', NumberType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Dépense annuelle de gros entretien et remplacement"
            ])
            ->add('cep', IntegerType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "CEP"
            ])
            ->add('carbonIndex', IntegerType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Emission CO2"
            ])
            ->add('carbonIndexTag', TextType::class, [
                'required' => false,
                'empty_data' =>"0",'label' => "Indice de carbone"
            ])
            ->add('confort', IntegerType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Confort"
            ])
            ->add('inheritanceGain', IntegerType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Valorisation du patrimoine"
            ])
            ->add('energyFinalTag', TextType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Etiquette énergétique"
            ])
            ->add('energyGain', PercentType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Gain énergétique",
                'scale' => 2, 'type' => 'integer'
            ])
            ->add('environmentalGain', PercentType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Gain environnemental",
                'scale' => 2, 'type' => 'integer'
            ])
            ->add('environmentalGainTag', TextType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Etiquette environnementale"
            ])
            ->add('labelGain', TextType::class, [
                'required' => false,
                'empty_data' => "0",
                'label' => "Label obtenu"
            ])
            ->add('timeReturn', NumberType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Temps de retour"
            ])
            ->add('investismentTtc', NumberType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Investissement TTC"
            ])
            ->add('investismentHt', NumberType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Investissement HT sans honoraire"
            ])
            ->add('loadsEnergyGain', NumberType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Economie de charges (énergie uniquement)"
            ])
            ->add('loadsGain', NumberType::class, [
                'required' => false,
                'empty_data' =>"0",
                'label' => "Economie de charges"
            ])
            ->add('workHasScenarios', CollectionType::class, [
                'entry_type' => WorkHasScenarioType::class,
                'required' => true,
                'label' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_options' => [
                  'coOwnership' => $options['coOwnership'],
                ]
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['coOwnership']);
        $resolver->setDefaults([
            'data_class' => Scenario::class,
        ]);
    }
}
