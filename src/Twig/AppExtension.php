<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 25/10/19
 * Time: 17:03
 */

// src/Twig/AppExtension.php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('static', [$this, 'staticChoices']),
        ];
    }

    public function staticChoices($class, $property)
    {
        if (property_exists($class, $property)) {
            return $class::$$property;
        }
        return null;
    }
}