<?php

namespace App\Service;

use Doctrine\ORM\EntityManager;
use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\CoOwnerShip\Owner;
use App\Entity\User\User;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Service\User\Generator;


class Validation {

    const BUILDING = 'building';
  const WORK = 'work';
  const LOT = 'lot';
  const OWNER = 'owner';
  const USER = 'user';
  const SCENARIO = 'scenario';

  private $manager;
  private $validator;
  private $errors = [];
  private $allErrors = [];

  public function __construct(EntityManager $manager, ValidatorInterface $validator) {
    $this->manager = $manager;
    $this->validator = $validator;

    $this->errors[self::BUILDING] = [];
      $this->errors[self::SCENARIO] = [];
      $this->errors[self::WORK] = [];
    $this->errors[self::LOT] = [];
    $this->errors[self::OWNER] = [];
    $this->errors[self::USER] = [];
  }

  public function get(string $table, $errors, $line) {
      foreach ($errors as $error) {
        array_push($this->errors[$table], '[Import l.'.$line.'] '.$error->getPropertyPath().': '.$error->getMessage());
        array_push($this->allErrors, '[Import l.'.$line.'] '.$error->getPropertyPath().': '.$error->getMessage());
      }
  }

  public function getAllErrors()
  {
    return $this->allErrors;
  }

  public function getErrors() {
    return $this->errors;
  }

  public function getRepository(string $entityName) {
    return $this->manager->getRepository($entityName);
  }

  public function persist(string $validation, $meta, ?int $line = null) {
    $errors = $this->validator->validate($meta);
    if(count($errors) > 0) {
      $this->get($validation, $errors, $line);
    } else {
      $this->manager->persist($meta);
    }
  }

  public function flush() {
    if(count($this->getAllErrors()) === 0) {
      $this->manager->flush();
    }
  }
}
