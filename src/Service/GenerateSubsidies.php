<?php

namespace App\Service;

use App\Entity\Grant\Grant;
use App\Entity\Scenario\WorkHasBuilding;
use Doctrine\ORM\EntityManagerInterface;
use FOS\Bundle\LuaJsonBundle\Processor\LuaJsonProcessor;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;
use Opis\JsonSchema\Exception as OpisException;

class GenerateSubsidies

{
    private $em;
    private $processor;
    private $serializer;
    private $assets;

    public function __construct($assets, EntityManagerInterface $entityManager, LuaJsonProcessor $jsonLuaProcessor, SerializerInterface $serializer)
    {
        $this->em = $entityManager;
        $this->processor = $jsonLuaProcessor;
        $this->serializer = $serializer;
        $this->assets = $assets;
    }

    public function generateSubsidies($owner, $scenario, Grant $grant)
    {
        // data
        $lot = $owner->getLots()[0];
        $building = $lot->getBuilding();
        $coOwnership = $building->getCoOwnership();
        $workHasBuildings = $this->em->getRepository(WorkHasBuilding::class)->findByBuildingScenario($building, $scenario)->getResult();

        // json serialization
        $context = SerializationContext::create();
        $context->setGroups(array('grant'));
        $context->setSerializeNull(true);
        $json = $this->serializer->serialize([
            "owner" => $owner,
            "scenario" => $scenario,
            "lot" => $lot,
            "building" => $building,
            "coOwnership" => $coOwnership,
            "workHasBuildings" => $workHasBuildings], 'json', $context);

//        dump($json);die;
        $schema = realpath($this->assets . 'jsonSchema.json');
//        dump($schema);die;
        $lua = $grant->getSetting();
//        dump($lua);die;
        /** @var Symfony\Component\DependencyInjection\ContainerInterface $container */
        try {
            $result = $this->processor->execute(
                $json,
                $schema,
                $lua
            );
        } catch(OpisException $e){
            $result = [];
            $result['errors'] = $e;
        }
        
        return $result;
    }

}
