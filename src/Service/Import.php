<?php
namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Import
{
    private $target;

    public function __construct($target)
    {
        $this->target = $target;
    }

    public function upload(UploadedFile $file)
    {
        $ext = $file->getClientOriginalExtension() ? $file->getClientOriginalExtension() : 'xlsx';
        $fileName = md5(uniqid()).'.'.$ext;

        try {
            $file->move($this->getTarget(), $fileName);
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        return $fileName;
    }

    public function getTarget()
    {
        return $this->target;
    }
}
