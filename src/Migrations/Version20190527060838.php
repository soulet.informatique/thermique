<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190527060838 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE lot (id INT AUTO_INCREMENT NOT NULL, number VARCHAR(255) NOT NULL, adress VARCHAR(255) NOT NULL, staircase VARCHAR(255) NOT NULL, etage VARCHAR(255) NOT NULL, door VARCHAR(255) NOT NULL, living_space INT NOT NULL, year_entry DATE NOT NULL, principal_residence TINYINT(1) NOT NULL, apartment_type VARCHAR(255) NOT NULL, fees TINYINT(1) NOT NULL, fees_amount INT NOT NULL, tax_credit TINYINT(1) NOT NULL, tax_credit_amount INT NOT NULL, zero_rate_borrowing INT NOT NULL, tantiemes_apartment INT NOT NULL, tantiemes_box_ext INT NOT NULL, tantiemes_cellar INT NOT NULL, tantiemes_common_on_total INT NOT NULL, tantiemes_common_on_shop INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE works (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, cost_ht INT NOT NULL, cost_ttc INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE lot');
        $this->addSql('DROP TABLE works');
    }
}
