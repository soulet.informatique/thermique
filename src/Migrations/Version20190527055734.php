<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190527055734 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE scenario (id INT AUTO_INCREMENT NOT NULL, anual_energy_expens INT NOT NULL, annual_current_maintenance_expens INT NOT NULL, annual_large_maintenance_expens INT NOT NULL, cep INT NOT NULL, carbon_index INT NOT NULL, carbon_index_tag VARCHAR(4) NOT NULL, confort INT NOT NULL, inheritance_gain INT NOT NULL, energy_final_tag VARCHAR(4) NOT NULL, energy_gain INT NOT NULL, environmental_gain INT NOT NULL, environmental_gain_tag VARCHAR(4) NOT NULL, environmental_gain_total INT NOT NULL, label_gain VARCHAR(255) NOT NULL, time_return DOUBLE PRECISION NOT NULL, investisment_ttc DOUBLE PRECISION NOT NULL, investisment_ht DOUBLE PRECISION NOT NULL, loads_energy_gain INT NOT NULL, loads_gain INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `grant` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, setting VARCHAR(255) NOT NULL, position INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE owner (id INT AUTO_INCREMENT NOT NULL, gender VARCHAR(10) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, compagny VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, mail VARCHAR(255) NOT NULL, family_member_number INT NOT NULL, children_number INT NOT NULL, marital_status VARCHAR(50) NOT NULL, retired TINYINT(1) NOT NULL, housing_situation VARCHAR(255) NOT NULL, main_residence TINYINT(1) NOT NULL, tax_revenue_one INT NOT NULL, tax_revenue_two INT NOT NULL, got_anah TINYINT(1) NOT NULL, sub_anah_amount INT NOT NULL, got_tax_credit TINYINT(1) NOT NULL, tax_credit_amount INT NOT NULL, got_eco_credit TINYINT(1) NOT NULL, eco_credit_amount INT NOT NULL, year_entry DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE scenario');
        $this->addSql('DROP TABLE `grant`');
        $this->addSql('DROP TABLE owner');
    }
}
