<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190527054343 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE owner (id INT AUTO_INCREMENT NOT NULL, gender VARCHAR(10) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, compagny VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, mail VARCHAR(255) NOT NULL, family_member_number INT NOT NULL, children_number INT NOT NULL, marital_status VARCHAR(50) NOT NULL, retired TINYINT(1) NOT NULL, housing_situation VARCHAR(255) NOT NULL, main_residence TINYINT(1) NOT NULL, tax_revenue_one INT NOT NULL, tax_revenue_two INT NOT NULL, got_anah TINYINT(1) NOT NULL, sub_anah_amount INT NOT NULL, got_tax_credit TINYINT(1) NOT NULL, tax_credit_amount INT NOT NULL, got_eco_credit TINYINT(1) NOT NULL, eco_credit_amount INT NOT NULL, year_entry DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE owner');
    }
}
