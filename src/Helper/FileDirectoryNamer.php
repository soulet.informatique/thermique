<?php

namespace App\Helper;

use Doctrine\Common\Util\ClassUtils;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;
use App\Entity\Document\UploadedFile;
use App\Entity\CoOwnerShip\CoOwnership;

/**
 * Description of FileDirectoryNamer
 *
 * @author Stéphane Soulet
 */
class FileDirectoryNamer implements DirectoryNamerInterface
{  
    public function directoryName($file, PropertyMapping $mapping): string
    {
        if($file->getCoOwnership()) {
            return sprintf("coOwnership/%s", $file->getCoOwnership()->getBusinessNumber());
        }
        return sprintf("general");
    }

}
