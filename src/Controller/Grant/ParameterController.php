<?php

namespace App\Controller\Grant;

use App\Entity\Grant\Parameter;
use App\Form\Grant\ParameterType;
use App\Repository\Grant\ParameterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/parameter/grant")
 */
class ParameterController extends AbstractController
{
    /**
     * @Route("/", name="param_grants_index", methods={"GET"})
     */
    public function index(ParameterRepository $grantRepository): Response
    {
        return $this->render('object/index.html.twig', [
            'grants' => $grantRepository->findAll(),
            'object' => 'param_grant',
            'reference' => null
        ]);
    }

    /**
     * @Route("/new", name="param_grant_new", methods={"GET","POST"})
     */
    public function new(Request $request)
    {
        $subsidy = new Parameter();
        $form = $this->createForm(ParameterType::class, $subsidy);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $task = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute('param_grants_index');
        }

        return $this->render('object/new.html.twig', [
            'object' => 'param_grant',
            'reference' => null,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="param_grant_show", methods={"GET"})
     */
    public function show($id)
    {
        $repo = $this->getDoctrine()->getRepository(Parameter::class);
        $grant = $repo->find($id);
        return $this->render('object/view.html.twig', [
            'object' => 'param_grant',
            'entity' => $grant,
            'reference' => null,
                    ]);
    }
    /**
     * @Route("/{id}/edit", name="param_grant_edit", methods={"GET","POST"})
     */
    public function edit(Parameter $grant, Request $request, $id)
    {
        $form = $this->createForm(ParameterType::class, $grant);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();
            return $this->redirectToRoute('param_grants_index');
        }

        return $this->render('object/edit.html.twig', [
            'entity' => $grant,
            'object' => 'param_grant',
            'reference' => null,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="param_grant_delete", methods={"DELETE"})
     */
    public function delete($id)
    {

        $repository = $this->getDoctrine()->getRepository(Parameter::class);
        $subsidyToDelete = $repository->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($subsidyToDelete);
        $entityManager->flush();
        return $this->redirectToRoute('param_grants_index');
    }
}