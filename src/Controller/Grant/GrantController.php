<?php

namespace App\Controller\Grant;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\CoOwnerShip\Owner;
use App\Entity\Scenario\Scenario;
use App\Entity\Grant\Grant;
use App\Form\Grant\GrantType;
use App\Form\Grant\AddGrantType;
use App\Form\Grant\TestGrantType;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\GenerateSubsidies;
use Symfony\Component\HttpKernel\KernelInterface;
use App\Repository\ScenarioRepository;

/**
 * @Route({
 *  "en": "/admin/project/{referenceId}",
 *  "fr": "/admin/projet/{referenceId}"
 * })
 * @ParamConverter("coOwnership", class=CoOwnership::class, options={"mapping": {"referenceId" = "id"}})
 */
class GrantController extends AbstractController
{
    private $appKernel;
    private $generateSubsidies;

    public function __construct(KernelInterface $appKernel, GenerateSubsidies $generateSubsidies)
    {
        $this->appKernel = $appKernel;
        $this->generateSubsidies = $generateSubsidies;
    }

    /**
     * @Route({
     *  "en": "/grants",
     *  "fr": "/grants"
     * }, name="grants_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, CoOwnership $coOwnership, int $maxItemsPerPage = 15): Response
    {
        $repository = $this->getDoctrine()->getRepository(Grant::class);
        $grants = $paginator->paginate(
            $coOwnership->getGrants(),
            $request->query->getInt('page', 1),
            $maxItemsPerPage
        );

        return $this->render('object/index.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'grants' => $grants,
            'object' => 'grant'
        ]);
    }
    
  /**
   * @Route({
   *  "en": "/grant/add",
   *  "fr": "/grant/ajouter"
   * }, name="grant_new")
   */
  public function addGrant(Request $request, CoOwnership $coOwnership)
  {
      $grant = null;
      $form = $this->createForm(AddGrantType::class);

      $form->handleRequest($request);


      if($form->isSubmitted() && $form->isValid()) {
          $param = $form->get('grant')->getData();
          $grant = new Grant();
          $grant->setName($param->getName());
          $grant->setSetting($param->getSetting());
          $grant->setPosition($form->get('position')->getData());
          $grant->setPopulation($param->getPopulation());
          $grant->setCoOwnership($coOwnership);
          $em = $this->getDoctrine()->getManager();
          $em->persist($grant);

          $em->flush();

          return $this->redirectToRoute('grant_edit', ['referenceId' => $coOwnership->getId(), 'id' => $grant->getId()]);
      }

      return $this->render('object/new.html.twig', [
          'reference' => $coOwnership,
        'form' => $form->createView(),
          'object' => 'grant',
          'entity' => $grant
      ]);
  }

    /**
     * @Route({
     *  "en": "/grant/{id}",
     *  "fr": "/grants/{id}"
     * }, name="grant_show", methods={"GET"})
     */
    public function show(Grant $grant, CoOwnership $coOwnership): Response
    {
        return $this->render('object/view.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'entity' => $grant,
            'object' => 'grant'
        ]);
    }

    /**
     * @Route({
     *  "en": "/grant/{id}/edit",
     *  "fr": "/grants/{id}/modifier"
     * }, name="grant_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Grant $grant, CoOwnership $coOwnership): Response
    {
        $form = $this->createForm(GrantType::class, $grant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('grants_index', [
                'referenceId' => $coOwnership->getId(),
            ]);
        }

        return $this->render('object/edit.html.twig', [
            'reference' => $coOwnership,
            'entity' => $grant,
            'form' => $form->createView(),
            'object' => 'grant'
        ]);
    }

    /**
     * @Route({
     *  "en": "/grant/{id}/delete",
     *  "fr": "/grants/{id}/supprimer"
     * }, name="grant_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Grant $grant, CoOwnership $coOwnership): Response
    {
        if ($this->isCsrfTokenValid('delete'.$grant->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($grant);
            $entityManager->flush();
        }

        return $this->redirectToRoute('grants_index', ['referenceId' => $coOwnership->getId()]);
    }

    protected function testOwner(Grant $grant, Owner $owner, CoOwnership $coOwnership)
    {
        $results = array();
        $repository = $this->getDoctrine()->getRepository(Scenario::class);
        $scenarios = $repository->findScenariosByCoOwnership($coOwnership)->getResult();
//        $jsonSchemaPath = $this->appKernel->getProjectDir(). '/assets/';
        foreach ($scenarios as $scenario) {
            $results[] = $this->generateSubsidies->generateSubsidies($owner, $scenario, $grant);
        }
        $test = [];
        $test['owner'] = $owner;
        $test['grant'] = $grant;
        $test['results'] = $results;
        return $test;
    }

    /**
     * @Route({
     *  "en": "/grant/{id}/test/{owner_id}",
     *  "fr": "/grant/{id}/test/{owner_id}"
     * }, name="grant_test", defaults={"owner_id" = null})
     * @ParamConverter("owner", class=Owner::class, options={"mapping": {"owner_id": "id"}})
     */
    public function test(Request $request, Grant $grant, Owner $owner = null, CoOwnership $coOwnership)
    {
        if($owner == null) {
            $form = $this->createForm(TestGrantType::class, null, ['coOwnership' => $coOwnership]);

            $form->handleRequest($request);


            if ($form->isSubmitted() && $form->isValid()) {
                $owner = $form->get('owner')->getData();

                return $this->redirectToRoute('grant_test', ['referenceId' => $coOwnership->getId(), 'id' => $grant->getId(), 'owner_id' => $owner->getId()]);
            }

            return $this->render('object/new.html.twig', [
                'reference' => $coOwnership,
                'form' => $form->createView(),
                'object' => 'grant',
                'entity' => $grant
            ]);
        }

        $test = $this->testOwner($grant, $owner, $coOwnership);

        $repository = $this->getDoctrine()->getRepository(Scenario::class);
        $scenarios = $repository->findScenariosByCoOwnership($coOwnership)->getResult();
        return $this->render('coOwnership/grant/test.html.twig', [
            'scenarios' => $scenarios,
            'test' => $test,
            'coOwnership' => $coOwnership
        ]);

    }

    /**
     * @Route({
     *  "en": "/grant/test_all",
     *  "fr": "/grant/test_all"
     * }, name="grant_test_all")
     */
    public function testAll(Request $request, CoOwnership $coOwnership)
    {
        $repository = $this->getDoctrine()->getRepository(Owner::class);
        $owners = $repository->findOwnerByCoOwnership($coOwnership, true)->getQuery()->getResult();

        $tests = [];
        foreach($owners as $owner) {
            foreach($coOwnership->getGrants() as $grant) {
                $tests[] = $this->testOwner($grant, $owner, $coOwnership);
            }
//            break;
        }

        $repository = $this->getDoctrine()->getRepository(Scenario::class);
        $scenarios = $repository->findScenariosByCoOwnership($coOwnership)->getResult();
        return $this->render('coOwnership/grant/test_all.html.twig', [
            'scenarios' => $scenarios,
            'tests' => $tests,
            'coOwnership' => $coOwnership
        ]);

    }

    protected function checkAScenario(Owner $owner, CoOwnership $coOwnership)
    {
        $repository = $this->getDoctrine()->getRepository(Scenario::class);

        $repository = $this->getDoctrine()->getRepository(Scenario::class);
        $scenarios = $repository->findScenariosByCoOwnership($coOwnership)->getResult();
        $results = [];
        $i = 0;
        foreach ($scenarios as $scenario) {
            $res_grants = [];
            $j = 0;
            foreach($coOwnership->getGrants() as $grant) {
                $res_grants[$j++] = 0;
            }
            $results[$i++] = $res_grants;
        }
        $grants = [];

            foreach($coOwnership->getGrants() as $grant) {
                $grants[] = $this->testOwner($grant, $owner, $coOwnership);
            }
//            dump($grants);die;
        $j = 0;
            foreach ($grants as $grant) {
                $i = 0;

                foreach ($grant['results'] as $scenario) {
//                    $results[$i * 10 + $j++] = $res->getData();
                    $results[$i++][$j] = $scenario->getData();
                }
                $j++;
            }
//        dump($results);die;
        return $results;
    }

    /**
     * @Route({
     *  "en": "/grant/check_scenarios",
     *  "fr": "/grant/check_scenarios"
     * }, name="grant_check_scenarios")
     */
    public function checkScenarios(Request $request, CoOwnership $coOwnership)
    {
        $repository = $this->getDoctrine()->getRepository(Scenario::class);
        $scenarios = $repository->findScenariosByCoOwnership($coOwnership)->getResult();
        $investment = [];

        $repository = $this->getDoctrine()->getRepository(Owner::class);

        $owners = $repository->findOwnerByCoOwnership($coOwnership, true)->getQuery()->getResult();
        $owner = null;
        $tantiemes = array(21,46,63,83,105);
        foreach($owners as $elt) {
            $owner = $elt;
            break;
        }
        $owner->setMaritalStatus(Owner::MARITAL_SINGLE);
//        dump($owner->getFirstLot()->getTantiemesApartment()/$owner->getFirstLot()->getBuilding()->getTantiemes());die;
        $owner->getFirstLot()->getBuilding()->setTantiemes(10000);
        $owner->setStep(3);
//        $owner->set
        foreach ($tantiemes as $tantieme) {
            $owner->setHousingSituation(Owner::HOUSING_USE);
            $owner->setTaxRevenueOne(20000);

                $owner->getFirstLot()->setTantiemesApartment($tantieme);
                $results[] = $this->checkAScenario($owner, $coOwnership);

        $owner->setTaxRevenueOne(22000);

            $owner->getFirstLot()->setTantiemesApartment($tantieme);
            $results[] = $this->checkAScenario($owner, $coOwnership);

        $owner->setTaxRevenueOne(25000);

            $owner->getFirstLot()->setTantiemesApartment($tantieme);
            $results[] = $this->checkAScenario($owner, $coOwnership);


        $owner->setHousingSituation(Owner::HOUSING_LESSOR);

            $owner->getFirstLot()->setTantiemesApartment($tantieme);
            $results[] = $this->checkAScenario($owner, $coOwnership);

            $res_invest = [];
            foreach ($scenarios as $scenario) {
                 $res_invest[] = 100 * floor($owner->getInvestment($scenario)/100);
            }
            $investment[] = $res_invest;
        }





        return $this->render('coOwnership/grant/check_scenarios.html.twig', [
            'results' => $results,
            'investment' => $investment,
            'scenarios' => $scenarios,
            'coOwnership' => $coOwnership,
            'occupants' => ['POTM','POM','POI','PB'],
            'tantiemes' =>$tantiemes,
            'grants' => $coOwnership->getGrants()
        ]);

    }

}
