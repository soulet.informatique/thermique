<?php

namespace App\Controller\Document;

use App\Entity\Document\DocumentCategory;
use App\Form\Document\ParameterType;
use App\Repository\Document\ParameterRepository;
use PhpParser\Comment\Doc;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/parameter/file")
 */
class ParameterController extends AbstractController
{
    /**
     * @Route("/", name="param_files_index", methods={"GET"})
     */
    public function index(ParameterRepository $fileRepository): Response
    {
        return $this->render('object/index.html.twig', [
            'files' => $fileRepository->findAll(),
            'object' => 'param_file',
            'reference' => null
        ]);
    }

    /**
     * @Route("/new", name="param_file_new", methods={"GET","POST"})
     */
    public function new(Request $request)
    {
        $subsidy = new DocumentCategory();
        $form = $this->createForm(ParameterType::class, $subsidy);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $task = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute('param_files_index');
        }

        return $this->render('object/new.html.twig', [
            'object' => 'param_file',
            'reference' => null,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="param_file_show", methods={"GET"})
     */
    public function show($id)
    {
        $repo = $this->getDoctrine()->getRepository(DocumentCategory::class);
        $file = $repo->find($id);
        return $this->render('object/view.html.twig', [
            'object' => 'param_file',
            'entity' => $file,
            'reference' => null,
                    ]);
    }
    /**
     * @Route("/{id}/edit", name="param_file_edit", methods={"GET","POST"})
     */
    public function edit(DocumentCategory $file, Request $request, $id)
    {
        $form = $this->createForm(ParameterType::class, $file);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();
            return $this->redirectToRoute('param_files_index');
        }

        return $this->render('object/edit.html.twig', [
            'entity' => $file,
            'object' => 'param_file',
            'reference' => null,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="param_file_delete", methods={"DELETE"})
     */
    public function delete($id)
    {

        $repository = $this->getDoctrine()->getRepository(DocumentCategory::class);
        $subsidyToDelete = $repository->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($subsidyToDelete);
        $entityManager->flush();
        return $this->redirectToRoute('param_files_index');
    }
}