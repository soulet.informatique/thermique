<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Form\Questionnaire\QuestIFLotType;
use App\Form\Questionnaire\QuestIFOwnerType;
use App\Form\Questionnaire\QuestIFSituationType;
use App\Form\Questionnaire\QuestIFSubventionType;
use App\Entity\CoOwnerShip\Lot;
use App\Entity\CoOwnerShip\Owner;
use App\Entity\User\User;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


/**
 * @Route({
 *  "en": "/user/{user_id}",
 *  "fr": "/utilisateur/{user_id}"
 * })
 * @ParamConverter("user", class=User::class, options={"mapping": {"user_id" = "id"}})
 * @security("is_granted('POST_EDIT', user)", message="Connectez-vous à votre espace pour accéder au questionnaire")
 */
class UserController extends AbstractController
{

     /**
     * @Route("/", name="user")
     */
    public function userHome(User $user)
    {
        return $this->render('user/user.html.twig', [
            'user_id' => $user->getId(),
            'owner' => $user->getOwner()
        ]);
    }

    /**
     * @Route({
     *  "en": "/questionnary/{step}",
     *  "fr": "/questionnaire/{step}"
     * }, name="questionnaire_owner")
     */
    public function addQuestOwner(Request $request, $step)
    {
        $user = $this->getUser();
        $owner = $user->getOwner();

        switch($step) {
            default:
            case 0:
            return $this->redirectToRoute('user', ['user_id' => $user->getId()]);
                break;
            case 1:
            case 2:
            $form = $this->createForm(QuestIFOwnerType::class, $owner, array(
                'step' => $step,
                'validation_groups' => ['step'.$step]));
                break;
            case 3:
                $repository = $this->getDoctrine()->getRepository(Lot::class);
                $lot = $repository->findLotByOwner($owner);
                $form = $this->createForm(QuestIFLotType::class, $lot);
                break;
            case 4:
                return $this->redirectToRoute('user_infos', ['user_id' => $user->getId()]);
                break;
        }

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $owner->setMaxStep($step);
            if($step == 1) {
                $user->setEmail($owner->getMail());
                $user->setEmailCanonical($owner->getMail());
            }
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('questionnaire_owner', ['user_id' => $user->getId(), 'step' => $step + 1]);
        }

        return $this->render('user/questionnaire.html.twig', [
            'form' => $form->createView(),
            'step' => $step,
            'user_id' => $user->getId(),
        ]);
    }

    /**
     * @Route({
     *  "en": "/questionnary/page2",
     *  "fr": "/questionnaire/page2"
     * }, name="questionnaire_situation")
     */
    public function addQuestSituation(Request $request)
    {
        $user = $this->getUser();
        $owner = $user->getOwner();
        $form = $this->createForm(QuestIFSituationType::class, $owner);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('questionnaire_bien', ['user_id' => $user->getId()]);
        }

        return $this->render('user/questionnaire.html.twig', [
            'form' => $form->createView(),
            'step' => 2,
            'title' => 'Votre situation',
            'subtitle' => '(données indispensables à la simulation)',
            'user_id' => $user->getId(),
        ]);
    }

    /**
     * @Route({
     *  "en": "/questionnary/page3",
     *  "fr": "/questionnaire/page3"
     * }, name="questionnaire_bien")
     */
    public function addQuestBien(Request $request)
    {
        $user = $this->getUser();
        $owner = $user->getOwner();

        $repository = $this->getDoctrine()->getRepository(Lot::class);
        $lot = $repository->findLotByOwner($owner);

        $form = $this->createForm(QuestIFLotType::class, $lot);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('user_infos', ['user_id' => $user->getId()]);
        }

        return $this->render('user/questionnaire.html.twig', [
            'form' => $form->createView(),
            'step' => 3,
            'title' => 'Votre bien',
            'subtitle' => '(données indispensables à la simulation)',
            'user_id' => $user->getId(),
        ]);
    }

    /**
     * @Route({
     *  "en": "/questionnary/page4",
     *  "fr": "/questionnaire/page4"
     * }, name="questionnaire_sub")
     */
    public function addQuestSub(Request $request)
    {
        $user = $this->getUser();
        $owner = $user->getOwner();
        $form = $this->createForm(QuestIFSubventionType::class, $owner);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('user_infos', ['user_id' => $user->getId()]);
        }

        return $this->render('user/questionnaire.html.twig', [
            'form' => $form->createView(),
            'step' => 4,
            'title' => 'Subventions et travaux énergétiques',
            'user_id' => $user->getId(),
        ]);
    }

    /**
     * @Route({
     * "en": "/summary",
     * "fr": "/recapitulatif"
     * }, name="user_infos")
     */
    public function userInfos(User $user)
    {
        $owner = $user->getOwner();
        $repository = $this->getDoctrine()->getRepository(Lot::class);
        return $this->render('user/info.html.twig', [
            'user' => $user,
            'lot' => $repository->findLotByOwner($owner),
        ]);
    }
}
