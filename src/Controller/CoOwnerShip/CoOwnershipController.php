<?php

namespace App\Controller\CoOwnerShip;

use App\Form\CoOwnershipType;
use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\CoOwnerShip\Owner;
use App\Entity\CoOwnerShip\Lot;
use App\Entity\Scenario\Work;
use App\Entity\Scenario\Scenario;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\KernelInterface;
use App\Form\UploadType;
use App\Service\Import;
use App\Service\Reader;
use App\Service\Parser;
use App\Service\User\Generator;
use App\Entity\Export\Upload;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route({
 *  "en": "/admin/project",
 *  "fr": "/admin/projet"
 * })
 */
class CoOwnershipController extends AbstractController
{

    private $contents;

    public function __construct(string $contents)
    {
        $this->contents = $contents;
    }
  /**
   * @Route({
   *  "en": "/add",
   *  "fr": "/ajouter"
   * }, name="coOwnership_add", methods={"GET","POST"})
   */
  public function new(Request $request): Response
  {
      $coOwnership = new CoOwnership();
      $form = $this->createForm(CoOwnershipType::class, $coOwnership);

      $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid()) {
          $em = $this->getDoctrine()->getManager();
          $em->persist($coOwnership);
          $em->flush();
          return $this->redirectToRoute('coOwnership_view', ['id' => $coOwnership->getId()]);
      }

      return $this->render('object/new.html.twig', [
          'object' => 'coOwnership',
          'reference' => null,
            'form' => $form->createView(),
      ]);
  }

  /**
   * @Route("/{id}", name="coOwnership_view", methods={"GET"})
   */
    public function view(PaginatorInterface $paginator, Request $request, CoOwnership $coOwnership): Response
    {
        $lotRepository = $this->getDoctrine()->getRepository(Lot::class);
        $ownerRepository = $this->getDoctrine()->getRepository(Owner::class);

        $lots = $lotRepository->findLotsByCoOwnership($coOwnership);
//        $lots = $paginator->paginate(
//            $lotRepository->findLotsByCoOwnership($coOwnership),
//            $request->query->getInt('pageLot', 1),
//            15,
//            ['pageParameterName' => 'pageLot', 'sortDirectionParameterName' => 'dirLot']
//        );
//        $lots->setParam('section', 'lot');
//        $owners = $ownerRepository->findOwnerByCoOwnership($coOwnership)->getQuery()->getResult();
        $owners = $paginator->paginate(
            $ownerRepository->findOwnerByCoOwnership($coOwnership),
            $request->query->getInt('pageOwner', 1),
            15,
            ['pageParameterName' => 'pageOwner', 'sortDirectionParameterName' => 'dirOwner']
        );

        $workRepository = $this->getDoctrine()->getRepository(Work::class);
        $works = $workRepository->findWorksByCoOwnership($coOwnership)->getResult();

        $scenarioRepository = $this->getDoctrine()->getRepository(Scenario::class);
        $scenarios = $scenarioRepository->findScenariosByCoOwnership($coOwnership)->getResult();

        return $this->render('coOwnership/view.html.twig', [
          'coOwnership' => $coOwnership,
          'lots' => $lots,
          'owners' => $owners,
            'works' => $works,
            'scenarios' => $scenarios
        ]);
    }

  /**
   * @Route({
   *  "en": "/{id}/edit",
   *  "fr": "/{id}/modifier"
   * }, name="coOwnership_edit", methods={"GET","POST"})
   */
  public function edit(Request $request, CoOwnership $coOwnership): Response
  {
      $form = $this->createForm(CoOwnershipType::class, $coOwnership);

      $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid()) {
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('coOwnership_view', ['id' => $coOwnership->getId()]);
      }

      return $this->render('coOwnership/edit.html.twig', [
          'object' => 'coOwnership',
          'entity' => $coOwnership,
          'reference' => null,
          'form' => $form->createView(),
      ]);
  }

  /**
   * @Route({
   *  "en": "/{id}/delete",
   *  "fr": "/{id}/supprimer"
   * }, name="coOwnership_delete", methods={"DELETE"})
   */
  public function delete(Request $request, CoOwnership $coOwnership): Response
  {
      if ($this->isCsrfTokenValid('delete'.$coOwnership->getId(), $request->request->get('_token'))){
        $em = $this->getDoctrine()->getManager();
        $em->remove($coOwnership);
        $em->flush();

        $this->addFlash('success', 'Projet supprimé avec succès!');
      }

     return $this->redirectToRoute('coOwnerships_index');
  }

    /**
     * @Route({
     *  "en": "/{id}/import/{type}",
     *  "fr": "/{id}/import/{type}"
     * }, name="coOwnership_import")
     */
    public function import(CoOwnership $coOwnership, $type, KernelInterface $kernel, Reader $reader, Request $request, Import $import, Parser $parser)
    {
        switch($type) {
            case 'owner': break;
            case 'work'; break;
            case 'scenario': break;
            default: throw $this->createNotFoundException('Page inconnue');
        }
        $upload = new Upload();
        $form = $this->createForm(UploadType::class, $upload);

        $form->handleRequest($request);
//        dump($request->getLocale());die;

        if($form->isSubmitted() && $form->isValid()) {
            $file = $upload->getFile();
            $fileName = $import->upload($file);
            $rootDir = $kernel->getProjectDir();
            $spreadsheet = $reader->readFile($this->contents.'/'.$fileName);
            $data = $reader->createDataFromSpreadsheet($spreadsheet, $type);
            $coOwnership->setFile($fileName);
            $parser->parse($data, $type);
            $error = $parser->send($coOwnership);

            return $this->render('object/import.html.twig', [
                'status' => 'block',
                'coOwnership' => $coOwnership,
                'form' => $form->createView(),
                'datas' => $parser->getSpreadsheet(),
                'errors' => $error->getAllErrors(),
                'header' => $parser->getHeader(),
                'object' => $type
            ]);


        }

        return $this->render('coOwnership/'.$type.'/import.html.twig', [
            'status' => 'none',
            'coOwnership' => $coOwnership,
            'form' => $form->createView(),
            'datas' => null,
            'errors' => null,
        ]);
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route({
     *  "en": "/{id}/generate_users",
     *  "fr": "/{id}/creer_utilisateurs"
     * }, name="generate_users")
     */
    public function generateUsers(Request $request, CoOwnership $coOwnership, Generator $generator): Response
    {
        if($coOwnership->getBlockAccounts()) {
            $this->addFlash('error', 'Les comptes sont bloqués');
            return $this->redirectToRoute('coOwnership_view', ['id' => $coOwnership->getId()]);
        }
        ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        set_time_limit(300);
        $results = $generator->generateUser($coOwnership, $coOwnership->getOwners());

//        dump($results);die;
//        dump($coOwnership->getOwners());die;
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $this->addFlash('success', 'Mots de passe générés avec succès!');
        return $this->render('object/index.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'results' => $results,
            'object' => 'user',
            'no_add' => true
        ]);
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/{id}/block", name="coOwnership_block", methods={"GET"})
     */
    public function block(Request $request, CoOwnership $coOwnership): Response
    {
        $coOwnership->setBlockAccounts(true);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $this->addFlash('success', 'Comptes bloqués');

        return $this->redirectToRoute('coOwnership_view', ['id' => $coOwnership->getId()]);
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/{id}/unblock", name="coOwnership_unblock", methods={"GET"})
     */
    public function unblock(Request $request, CoOwnership $coOwnership): Response
    {
        $coOwnership->setBlockAccounts(false);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $this->addFlash('success', 'Comptes débloqués');

        return $this->redirectToRoute('coOwnership_view', ['id' => $coOwnership->getId()]);
    }
}
