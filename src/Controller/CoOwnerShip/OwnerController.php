<?php

namespace App\Controller\CoOwnerShip;

use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\CoOwnerShip\Owner;
use App\Form\OwnerType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Knp\Component\Pager\PaginatorInterface;



/**
 * @Route({
 *  "en": "/admin/project/{referenceId}",
 *  "fr": "/admin/projet/{referenceId}"
 * })
 * @ParamConverter("coOwnership", class=CoOwnership::class, options={"mapping": {"referenceId" = "id"}})
 */
class OwnerController extends AbstractController
{
    /**
     * @Route({
     *  "en": "/owners",
     *  "fr": "/proprietaires"
     * }, name="owners_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, CoOwnership $coOwnership): Response
    {

        $repository = $this->getDoctrine()->getRepository(Owner::class);
        $owners = $paginator->paginate(
          $repository->findOwnerByCoOwnership($coOwnership),
          $request->query->getInt('pageOwner', 1),
          15,
            ['pageParameterName' => 'pageOwner', 'sortDirectionParameterName' => 'dirOwner']
        );

        return $this->render('object/index.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'owners' => $owners,
            'object' => 'owner',
            'pagination' => $owners
        ]);
    }

    /**
     * @Route({
     *  "en": "/owner/add",
     *  "fr": "/proprietaire/ajouter"
     * }, name="owner_new", methods={"GET","POST"})
     */
    public function new(Request $request, CoOwnership $coOwnership): Response
    {
        $owner = new Owner();
        $form = $this->createForm(OwnerType::class, $owner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($owner);
            $entityManager->flush();

            return $this->redirectToRoute('owners_index', ['referenceId' => $coOwnership->getId()]);
        }

        return $this->render('object/new.html.twig', [
            'reference' => $coOwnership,
            'entity' => $owner,
            'form' => $form->createView(),
            'object' => 'owner'
        ]);
    }

    /**
     * @Route({
     *  "en": "/owner/{id}",
     *  "fr": "/proprietaire/{id}"
     * }, name="owner_show", methods={"GET"})
     */
    public function show(Owner $owner, CoOwnership $coOwnership): Response
    {
        return $this->render('object/view.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'entity' => $owner,
            'object' => 'owner'
        ]);
    }

    /**
     * @Route({
     *  "en": "/owner/{id}/edit",
     *  "fr": "/proprietaire/{id}/modifier"
     * }, name="owner_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Owner $owner, CoOwnership $coOwnership): Response
    {
        $form = $this->createForm(OwnerType::class, $owner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('owners_index', [
                'referenceId' => $coOwnership->getId(),
            ]);
        }

        return $this->render('object/edit.html.twig', [
            'reference' => $coOwnership,
            'entity' => $owner,
            'form' => $form->createView(),
            'object' => 'owner'
        ]);
    }

    /**
     * @Route({
     *  "en": "/owner/{id}/delete",
     *  "fr": "/proprietaire/{id}/supprimer"
     * }, name="owner_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Owner $owner, CoOwnership $coOwnership): Response
    {
        if ($this->isCsrfTokenValid('delete'.$owner->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($owner);
            $entityManager->flush();
        }

        return $this->redirectToRoute('owners_index', ['referenceId' => $coOwnership->getId()]);
    }

    /**
     * @Route({
     *  "en": "/delete_all",
     *  "fr": "/supprimer_tous"
     * }, name="owner_delete_all", methods={"GET"})
     */
    public function deleteAll(Request $request, CoOwnership $coOwnership): Response
    {

            $entityManager = $this->getDoctrine()->getManager();

        foreach($coOwnership->getBuildings() as $building) {
            foreach($building->getLots() as $lot) {
                $entityManager->remove($lot);
            }
        }
            $entityManager->flush();


        return $this->redirectToRoute('owners_index', ['referenceId' => $coOwnership->getId()]);
    }
}
