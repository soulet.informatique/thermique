<?php

namespace App\Controller\CoOwnerShip;

use App\Entity\CoOwnerShip\Building;
use App\Entity\CoOwnerShip\CoOwnership;
use App\Form\BuildingType;
use App\Repository\BuildingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route({
 *  "en": "/admin/project/{referenceId}",
 *  "fr": "/admin/projet/{referenceId}"
 * })
 * @ParamConverter("coOwnership", class=CoOwnership::class, options={"mapping": {"referenceId" = "id"}})
 */
class BuildingController extends AbstractController
{
    /**
     * @Route({
     *  "en": "/buildings",
     *  "fr": "/batiments"
     * }, name="buildings_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, CoOwnership $coOwnership): Response
    {
        $repository = $this->getDoctrine()->getRepository(Building::class);
        $buildings = $paginator->paginate(
          $repository->findBuildingByCoOwnership($coOwnership),
          $request->query->getInt('page', 1),
          15
        );

        return $this->render('object/index.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'buildings' => $buildings,
            'object' => 'building'
        ]);
    }

    /**
     * @Route({
     *  "en": "/building/add",
     *  "fr": "/batiment/ajouter"
     * }, name="building_new", methods={"GET","POST"})
     */
    public function new(Request $request, CoOwnership $coOwnership): Response
    {
        $building = new Building();
        $form = $this->createForm(BuildingType::class, $building);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($building);
            $entityManager->flush();

            return $this->redirectToRoute('buildings_index');
        }

        return $this->render('object/new.html.twig', [
            'reference' => $coOwnership,
            'building' => $building,
            'form' => $form->createView(),
            'object' => 'building'
        ]);
    }

    /**
     * @Route({
     *  "en": "/building/{id}",
     *  "fr": "/batiment/{id}"
     * }, name="building_show", methods={"GET"})
     */
    public function show(Building $building, CoOwnership $coOwnership): Response
    {
        return $this->render('object/view.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'entity' => $building,
            'object' => 'building',
        ]);
    }

    /**
     * @Route({
     *  "en": "/building/{id}/edit",
     *  "fr": "/batiment/{id}/modifier"
     * }, name="building_edit", methods={"GET","POST"})
     */
    public function edit(Building $building, Request $request, CoOwnership $coOwnership): Response
    {
        $form = $this->createForm(BuildingType::class, $building);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('buildings_index', [
                'referenceId' => $coOwnership->getId(),
            ]);
        }

        return $this->render('object/edit.html.twig', [
            'reference' => $coOwnership,
            'form' => $form->createView(),
            'object' => 'building',
            'entity' => $building
        ]);
    }

    /**
     * @Route({
     *  "en": "/building/{id}/delete",
     *  "fr": "/batiment/{id}/supprimer"
     * }, name="building_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CoOwnership $coOwnership, Building $building): Response
    {
        if ($this->isCsrfTokenValid('delete'.$building->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($building);
            $entityManager->flush();
        }

        return $this->redirectToRoute('building_index', ['referenceId' => $coOwnership->getId()]);
    }
}
