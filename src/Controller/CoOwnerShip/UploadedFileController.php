<?php

namespace App\Controller\CoOwnerShip;

use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\Document\UploadedFile;
use App\Form\Document\UploadedFileType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

/**
 * @Route({
 *  "en": "/admin/project/{referenceId}",
 *  "fr": "/admin/projet/{referenceId}"
 * })
 * @ParamConverter("coOwnership", class=CoOwnership::class, options={"mapping": {"referenceId" = "id"}})
 */
class UploadedFileController extends AbstractController
{
    private $vichUploader;

    public function __construct(UploaderHelper $vichUploader)
    {
        $this->vichUploader = $vichUploader;
    }

    /**
     * @Route("/uploadedFiles", name="uploadedFiles_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, CoOwnership $coOwnership): Response
    {
        $repository = $this->getDoctrine()->getRepository(UploadedFile::class);
        $uploadedFiles = $paginator->paginate(
          $repository->findDocuments(['coOwnership' => $coOwnership]),
          $request->query->getInt('pageUploadedFile', 1),
          15,
            ['pageParameterName' => 'pageUploadedFile']
        );

        return $this->render('object/index.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'uploadedFiles' => $uploadedFiles,
            'object' => 'uploadedFile',
            'pagination' => $uploadedFiles
        ]);
    }

    /**
     * @Route({
     *  "en": "/uploadedFile/add",
     *  "fr": "/uploadedFile/ajouter"
     * }, name="uploadedFile_new", methods={"GET","POST"})
     */
    public function new(Request $request, CoOwnership $coOwnership): Response
    {
        $uploadedFile = new UploadedFile();
        $uploadedFile->setCoOwnership($coOwnership);
        $form = $this->createForm(UploadedFileType::class, $uploadedFile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($uploadedFile);
            $entityManager->flush();

            return $this->redirectToRoute('uploadedFiles_index', ['referenceId' => $coOwnership->getId()]);
        }

        return $this->render('object/new.html.twig', [
            'reference' => $coOwnership,
            'entity' => $uploadedFile,
            'form' => $form->createView(),
            'object' => 'uploadedFile'
        ]);
    }

    /**
     * @Route("/uploadedFile/{id}", name="uploadedFile_show", methods={"GET"})
     */
    public function show(UploadedFile $uploadedFile, CoOwnership $coOwnership): Response
    {
//        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        $path = $this->vichUploader->asset($uploadedFile, 'file');
        $response = new Response(file_get_contents($uploadedFile->getFile()));
        $response->headers->set('Content-Type', mime_content_type($path));
        $response->headers->set('Content-Disposition', 'inline;filename="' . $uploadedFile->getOriginalFileName());
        return $response;
    }

    /**
     * @Route({
     *  "en": "/uploadedFile/{id}/edit",
     *  "fr": "/uploadedFile/{id}/modifier"
     * }, name="uploadedFile_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UploadedFile $uploadedFile, CoOwnership $coOwnership): Response
    {
        $form = $this->createForm(UploadedFileType::class, $uploadedFile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('uploadedFiles_index', [
                'referenceId' => $coOwnership->getId(),
            ]);
        }

        return $this->render('object/edit.html.twig', [
            'reference' => $coOwnership,
            'entity' => $uploadedFile,
            'form' => $form->createView(),
            'object' => 'uploadedFile'
        ]);
    }

    /**
     * @Route({
     *  "en": "/uploadedFile/{id}/delete",
     *  "fr": "/uploadedFile/{id}/supprimer"
     * }, name="uploadedFile_delete", methods={"DELETE"})
     */
    public function delete(Request $request, UploadedFile $uploadedFile, CoOwnership $coOwnership): Response
    {
        if ($this->isCsrfTokenValid('delete'.$uploadedFile->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($uploadedFile);
            $entityManager->flush();
        }

        return $this->redirectToRoute('uploadedFiles_index', ['referenceId' => $coOwnership->getId()]);
    }
}
