About Thermique
===============

Thermique est un applicatif web facilitant le partage aux propriétaires/co-propriétaires des études thermiques de bâtiments et particulièrement l'ingénierie financière liée.

Thermique est sous licence GNU AFFERO GENERAL PUBLIC LICENSE Version 3.

Thermique a été développé sur Symfony 4.

# Fonctionnalités

* Gestion de co-propriétés
* Génération automatique de document avec multiple scénario
* Paramétrage de subvention
* Gestion de compte par propriétaire/co-propriétaire

# Note

Lire les notes d'installation dans INSTALL.md

# Contribute

Ce projet a initialement été proposé par l'entreprise 3DNova dans le cadre d'une formation Dev Web (Fabrique Simplon.co, Chambéry Promo #1).

